package br.es.gov.cachoeiro.commonshibernate.exception.login;



import br.es.gov.cachoeiro.commons.validator.Validator;
import br.es.gov.cachoeiro.commonshibernate.exception.ServiceException;


public class NescessarioAlterarSenhaException extends ServiceException {

	private final static String msg = "É necessário alterar a senha deste usuário.";

	public NescessarioAlterarSenhaException() {
		super();
	}

	/**
	 * @param message
	 */
	public NescessarioAlterarSenhaException(String message) {
		super(message != null ? message : msg);
	}

	public NescessarioAlterarSenhaException(String message, int codigo) {
		super(!Validator.isEmptyOrNull(message) ? message : msg);
		this.codigo = codigo;
	}

	/**
	 * @param cause
	 */
	public NescessarioAlterarSenhaException(Throwable cause) {
		super(cause);
	}

	/**
	 * @param message
	 * @param cause
	 */
	
	
	public NescessarioAlterarSenhaException(String message, Throwable cause) {
		super(!Validator.isEmptyOrNull(message) ? message : msg, cause);
	}

	private int codigo;

	public int getCodigo() {
		return codigo;
	}

	public void setCodigo(int codigo) {
		this.codigo = codigo;
	}

}
