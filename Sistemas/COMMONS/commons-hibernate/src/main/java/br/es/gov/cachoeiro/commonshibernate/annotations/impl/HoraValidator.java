package br.es.gov.cachoeiro.commonshibernate.annotations.impl;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.hibernate.validator.Validator;


import br.es.gov.cachoeiro.commonshibernate.annotations.Hora;

public class HoraValidator implements Validator<Hora> {

	public void initialize(Hora parameters) {
	}

	public boolean isValid(Object value) {

		if (value == null) {
			return true;
		}
		String valor = String.valueOf(value);
		
		String reg = "^\\d{2}(:\\d{2})?$||()";
		
		Pattern pattern = Pattern.compile(reg);
		Matcher match = pattern.matcher(valor);

		return match.matches();
	}

	
}

