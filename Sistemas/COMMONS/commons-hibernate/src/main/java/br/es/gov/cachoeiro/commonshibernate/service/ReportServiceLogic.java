package br.es.gov.cachoeiro.commonshibernate.service;

import java.util.List;

import br.es.gov.cachoeiro.commonshibernate.exception.ServiceException;

public interface  ReportServiceLogic<E> {
	
	List<E> gerar(String... args) throws ServiceException;

}