package br.es.gov.cachoeiro.commonshibernate.exception;

import br.es.gov.cachoeiro.commons.validator.Validator;

public class PermissaoNegadaException extends ServiceException {

	private final static String msg = "Usuário não tem permissão para acessar a função.";

	public PermissaoNegadaException() {
		super();
	}

	/**
	 * @param message
	 */
	public PermissaoNegadaException(String message) {
		super(message != null ? message : msg);
	}

	public PermissaoNegadaException(String message, int codigo) {
		super(!Validator.isEmptyOrNull(message) ? message : msg);
		this.codigo = codigo;
	}

	/**
	 * @param cause
	 */
	public PermissaoNegadaException(Throwable cause) {
		super(cause);
	}

	/**
	 * @param message
	 * @param cause
	 */
	public PermissaoNegadaException(String message, Throwable cause) {
		super(!Validator.isEmptyOrNull(message) ? message : msg, cause);
	}

	private int codigo;

	public int getCodigo() {
		return codigo;
	}

	public void setCodigo(int codigo) {
		this.codigo = codigo;
	}

}
