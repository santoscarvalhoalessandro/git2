package br.es.gov.cachoeiro.commonshibernate.annotations.impl;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.hibernate.validator.Validator;


import br.es.gov.cachoeiro.commonshibernate.annotations.Cep;

public class CepValidator implements Validator<Cep>{

	public void initialize(Cep parameters) {
	}

	public boolean isValid(Object value) {

		if (value == null) {
			return true;
		}
		String valor = String.valueOf(value);
		
		String reg = "^\\d{5}(-\\d{3})?$||()";
		
		Pattern pattern = Pattern.compile(reg);
		Matcher match = pattern.matcher(valor);

		return match.matches();
	}
  
}