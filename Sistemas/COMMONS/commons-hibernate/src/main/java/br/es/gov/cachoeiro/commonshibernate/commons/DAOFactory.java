package br.es.gov.cachoeiro.commonshibernate.commons;

import java.lang.reflect.Constructor;
import java.lang.reflect.Method;
import java.util.Properties;

import org.hibernate.Session;

import br.es.gov.cachoeiro.commonshibernate.dao.DaoBase;
import br.es.gov.cachoeiro.commonshibernate.exception.DaoException;

public class DAOFactory {
	private static DAOFactory singleton;

	private Properties daosProperties = null;

	// private HashMap<Object, Object> daoCache = null;
	private Properties daoProperties = null;

	private DAOFactory() {
		// daoCache = new HashMap<Object, Object>();

		daoProperties = new Properties();
		/*
		 * try { InputStream input =
		 * DAOFactory.class.getResourceAsStream("/daoRegistry.properties");
		 * if(input!=null) { daoProperties.load(input); } } catch (IOException
		 * e) { new ServiceException(
		 * "A lista de DAOs registradas nao foi carregada", e.getCause()); }
		 */

	}

	public static DAOFactory getInstance() {
		if (singleton == null) {
			singleton = new DAOFactory();
		}
		return singleton;
	}

	public DaoBase getDAO(Class daoInterface, ClassLoader classLoader) throws DaoException {
		DaoBase dao = null;
		String name = null;

		// if (!daoCache.containsKey(daoInterface)) {

		if (daoProperties.getProperty(daoInterface.getName()) != null) {
			name = daoProperties.getProperty(daoInterface.getName());
		} else {
			String subpack = "impl.";

			name = daoInterface.getName();
			name = name.substring(0, name.lastIndexOf(".") + 1) + subpack
					+ name.substring(name.lastIndexOf(".") + 1, name.length()) + "Impl";
		}

		try {
			// System.out.println("Class name: "+name);
			if (classLoader == null) {
				dao = (DaoBase) Class.forName(name).newInstance();
			} else {
				Class cl = classLoader.loadClass(name);
				dao = (DaoBase) cl.newInstance();
			}
			// daoCache.put(daoInterface, dao);
		} catch (ClassNotFoundException e) {
			throw new DaoException("A DAO " + name + " não foi encontrada.", e);
		} catch (Exception e) {
			throw new DaoException("Erro ao carregar a DAO " + name, e);
		}
		// }

		return (DaoBase) dao;
		// return (DaoBase) daoCache.get(daoInterface);
	}

	public DaoBase getDAO(Class daoInterface) throws DaoException {
		DaoBase dao = getDAO(daoInterface, (ClassLoader) null);
		return dao;
	}

	public DaoBase getDAO(Class daoInterface, Session s) throws DaoException {
		DaoBase dao = getDAO(daoInterface, (ClassLoader) null);
		return dao;
	}

	public DaoBase getDAOPassingSession(Class daoInterface, Session s) throws DaoException {

		DaoBase dao = null;
		String name = null;

		Class daoClass = null;
		Constructor constructor = null;

		if (daoProperties.getProperty(daoInterface.getName()) != null) {
			name = daoProperties.getProperty(daoInterface.getName());
		} else {
			String subpack = "impl.";

			name = daoInterface.getName();
			name = name.substring(0, name.lastIndexOf(".") + 1) + subpack
					+ name.substring(name.lastIndexOf(".") + 1, name.length()) + "Impl";
		}

		try {
			daoClass = Class.forName(name);

			constructor = daoClass.getConstructor(new Class[] { Session.class });
			dao = (DaoBase) constructor.newInstance(new Object[] { s });
		} catch (ClassNotFoundException e) {
			throw new DaoException("A DAO " + name + " não foi encontrada.", e);
		} catch (Exception e) {
			throw new DaoException("Erro ao carregar a DAO " + name, e);
		}
		return dao;
	}

	public DaoBase getDAOWithAuditing(Class daoInterface, String sys, String user) throws DaoException {
		DaoBase dao = getDAO(daoInterface);

		Method meth;
		try {
			meth = dao.getClass().getMethod("setAuditData", new Class[] { String.class, String.class });
			Object retobj = meth.invoke(dao, new Object[] { sys, user });
		} catch (Exception e) {
			e.printStackTrace();
		}

		return dao;
	}

	public DaoBase getDAOWithAuditingAndSession(Class daoInterface, String sys, String user, Session s)
			throws DaoException {
		DaoBase dao = getDAOPassingSession(daoInterface, s);

		Method meth;
		try {
			meth = dao.getClass().getMethod("setAuditData", new Class[] { String.class, String.class });
			Object retobj = meth.invoke(dao, new Object[] { sys, user });
		} catch (Exception e) {
			e.printStackTrace();
		}

		return dao;
	}
}