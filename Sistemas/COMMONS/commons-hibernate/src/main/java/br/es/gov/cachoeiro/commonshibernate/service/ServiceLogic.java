package br.es.gov.cachoeiro.commonshibernate.service;


import java.io.Serializable;
import java.util.List;
import java.util.Map;

import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.transform.ResultTransformer;

import br.es.gov.cachoeiro.commonshibernate.exception.ServiceException;

public interface ServiceLogic<E, PK> {

	void setAuditData(String sys, String user);

	// public void setDaoManager(BaseDaoManager daoManager);
	// public BaseDaoManager getDaoManager();
	public abstract Class<E> getEntityClass();

	public abstract Class getDaoClass();

	public E save(E entity) throws ServiceException;

	public void update(E entity) throws ServiceException;

	public E saveOrUpdate(E entity) throws ServiceException;

	public void refresh(final E entity) throws ServiceException;

	public void delete(final E entity) throws ServiceException;

	public E get(final PK primaryKey) throws ServiceException;

	/*
	 * metodos para consultas
	 */
	List<E> listAll() throws ServiceException;

	List<E> listAll(final int firstResult, final int maxResult) throws ServiceException;

	E getInstance(final PK primaryKey) throws ServiceException;

	List<E> findByCriteria(Criterion... criterion) throws ServiceException;

	List<E> findByExample(E entity) throws ServiceException;

	List<E> findByCriteria(DetachedCriteria criteria) throws ServiceException;

	List<E> findByCriteria(int first, int max, DetachedCriteria criteria) throws ServiceException;

	List<E> consultaPadrao(String... args) throws ServiceException;

	List<E> consultaPadrao(int first, int max, String... args) throws ServiceException;

	List<E> pesquisaPadrao(String... args) throws ServiceException;

	List<E> pesquisaPadrao(int first, int max, String... args) throws ServiceException;
	
	List<E> listCombobox(String... args) throws ServiceException;

	List listAnotherQuery(DetachedCriteria criteria) throws ServiceException;

	List findByHql(String hql) throws ServiceException;

	List findByHql(String hql, ResultTransformer rT) throws ServiceException;

	List findByHql(String hql, ResultTransformer rT, Map<String, Object> params) throws ServiceException;

	void setUsuarioLogado(Serializable usuarioLogado) throws ServiceException;

	public void executeSql(String sql) throws ServiceException;

	List querySql(String sql, String[] tipos) throws ServiceException;

}
