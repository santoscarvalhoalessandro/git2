package br.es.gov.cachoeiro.commonshibernate.annotations.impl;


import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.hibernate.validator.Validator;


import br.es.gov.cachoeiro.commonshibernate.annotations.TamanhoMax;

public class TamanhoMaxValidator implements Validator<TamanhoMax> {

	private int maxValid;

	public void initialize(TamanhoMax parameters) {
		maxValid = parameters.max();
	}

	public boolean isValid(Object value) {
		if (value == null) {
			return true;
		}
		String valor = String.valueOf(value);

		/**
		 * O objetivo e limitar a String recebida(valor) na quantidade de caracteres desejada(maxValid) 
		 * O 1o '^' significa que a busca vai ser feita des do inicio da String.
		 * A Expressao '[^A]' significa que todos os caracteres irao ser buscados, exeto o 'A'.
		 * A expressao '[A]' siginifica que ira buscar inclusive o 'A'.
		 * 
		 * '([^A]|[A])' (Nao e a forma correta, mas nao encontramos uma forma melhor) 
		 * 
		 * '||()' Se o campo estiver vazio, nao faz a critica
		 * 
		 */
		
		String reg = "^([^A]|[A]){1,"+maxValid+"}$||()";
		
		Pattern pattern = Pattern.compile(reg);
		Matcher match = pattern.matcher(valor);

		return match.matches();
	}

}
