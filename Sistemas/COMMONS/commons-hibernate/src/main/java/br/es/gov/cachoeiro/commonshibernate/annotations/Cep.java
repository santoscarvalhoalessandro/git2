package br.es.gov.cachoeiro.commonshibernate.annotations;


import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import br.es.gov.cachoeiro.commonshibernate.annotations.impl.CepValidator;
import org.hibernate.validator.ValidatorClass;




@ValidatorClass(CepValidator.class)
@Target({ElementType.FIELD, ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface Cep {
	
	
	
	String message() default "Cep Inválido. A máscara correta é '99999-999'.";
    
}
