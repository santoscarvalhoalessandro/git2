package br.es.gov.cachoeiro.commonshibernate.commons;


import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;

import javax.persistence.Id;

public class CommonsAnnotation<T> {

	public Object getIdValue(T entity) throws Exception {		
		Class<T> classeT = (Class<T>) ((ParameterizedType) getClass()
				.getGenericSuperclass()).getActualTypeArguments()[0];
		
		String getPrimaryKey;
		String primaryKey = null;
		
		Field[] fields = classeT.getDeclaredFields();
		for (Field field : fields) {
			if (field.isAnnotationPresent(Id.class)) {
				primaryKey = field.getName();
				getPrimaryKey = "get"+primaryKey.substring(0, 1).toUpperCase() + primaryKey.substring(1);
				Method meth = entity.getClass().getMethod(getPrimaryKey, null);
				Object retobj = meth.invoke(entity, null);
				return retobj;
			}
		}
		
		return null;
	}	
}

