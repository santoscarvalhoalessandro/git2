package br.es.gov.cachoeiro.commonshibernate.exception.login;


import br.es.gov.cachoeiro.commons.validator.Validator;
import br.es.gov.cachoeiro.commonshibernate.exception.ServiceException;

public class UsuarioNaoCadastradoException extends ServiceException {

	private final static String msg = "Usuario ou senha não existe.";

	public UsuarioNaoCadastradoException() {
		super();
	}

	/**
	 * @param message
	 */
	public UsuarioNaoCadastradoException(String message) {
		super(!Validator.isEmptyOrNull(message) ? message : msg);
	}

	public UsuarioNaoCadastradoException(String message, int codigo) {
		super(!Validator.isEmptyOrNull(message) ? message : msg);
		this.codigo = codigo;
	}

	/**
	 * @param cause
	 */
	public UsuarioNaoCadastradoException(Throwable cause) {
		super(cause);
	}

	/**
	 * @param message
	 * @param cause
	 */
	public UsuarioNaoCadastradoException(String message, Throwable cause) {
		super(!Validator.isEmptyOrNull(message) ? message : msg, cause);
	}

	private int codigo;

	public int getCodigo() {
		return codigo;
	}

	public void setCodigo(int codigo) {
		this.codigo = codigo;
	}

}
