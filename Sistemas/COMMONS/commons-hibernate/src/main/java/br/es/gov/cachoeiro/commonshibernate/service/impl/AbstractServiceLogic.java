package br.es.gov.cachoeiro.commonshibernate.service.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import org.apache.commons.beanutils.BeanComparator;
import org.apache.commons.collections.comparators.ComparatorChain;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.transform.ResultTransformer;
import org.hibernate.validator.ClassValidator;


import org.hibernate.validator.InvalidValue;

import br.es.gov.cachoeiro.commons.validator.Validator;
import br.es.gov.cachoeiro.commonshibernate.commons.DAOFactory;
import br.es.gov.cachoeiro.commonshibernate.commons.ServiceLocator;
import br.es.gov.cachoeiro.commonshibernate.dao.DaoBase;
import br.es.gov.cachoeiro.commonshibernate.exception.DaoException;
import br.es.gov.cachoeiro.commonshibernate.exception.ServiceException;
import br.es.gov.cachoeiro.commonshibernate.service.ServiceLogic;
import br.es.gov.cachoeiro.commonshibernate.util.HibernateValidator;

public abstract class AbstractServiceLogic<E, PK> implements ServiceLogic<E, PK> {

	public abstract Class getDaoClass();

	public abstract Class<E> getEntityClass();

	protected String sys;
	protected String user;
	private Serializable usuarioLogado;

	public void setAuditData(String sys, String user) {
		this.sys = sys;
		this.user = user;
		// System.out.println("setAuditData Sys: "+sys+"User: "+user);
	}

	public E get(PK primaryKey) throws ServiceException {
		E entity = null;
		try {
			if (primaryKey == null) {
				throw new NullPointerException("get->primaryKey");
			}
			entity = (E) getDAO().get(primaryKey);
			entity = afterGetRecord(entity);
		} catch (Exception e) {
			throw new ServiceException("Erro ao obter registro " + primaryKey, e);
		}
		return entity;
	}

	public E save(E entity) throws ServiceException {
		E ent = null;
		try {
			beforeSaveAndSaveOrUpdate(entity);
			beforeSave(entity);
			HibernateValidator.validateRequiredField(entity);
			ent = (E) getDAO().save(entity);
			afterSave(ent);
			afterSaveAndSaveOrUpdate(ent);
		} catch (Exception e) {
			throw new ServiceException("Erro ao salvar registro: " + e.getMessage(), e);
		}
		return ent;
	}

	public void update(E entity) throws ServiceException {
		beforeSaveAndSaveOrUpdate(entity);

		HibernateValidator.validateRequiredField(entity);
		try {
			getDAO().update(entity);
			afterSaveAndSaveOrUpdate(entity);
		} catch (Exception e) {
			throw new ServiceException("Erro ao alterar registro: " + e.getMessage(), e);
		}
	}

	public E saveOrUpdate(E entity) throws ServiceException {
		E ent = null;

		try {
			beforeSaveAndSaveOrUpdate(entity);

			HibernateValidator.validateRequiredField(entity);

			ent = (E) getDAO().saveOrUpdate(entity);
			afterSaveAndSaveOrUpdate(ent);
		} catch (Exception e) {
			e.printStackTrace();
			throw new ServiceException("Erro ao salvar/alterar registro: " + e.getMessage(), e);
		}
		return ent;
	}

	public void refresh(final E entity) throws ServiceException {
		try {
			getDAO().refresh(entity);
		} catch (Exception e) {
			throw new ServiceException("Erro ao revalidar registro ", e);
		}
	}

	public void delete(final E entity) throws ServiceException {
		HibernateValidator.validateOneToManyUsed(entity);
		beforeDelete(entity);
		try {
			getDAO().delete(entity);
			afterDelete(entity);
		} catch (Exception e) {
			throw new ServiceException("Erro ao excluir registro: " + e.getMessage(), e);
		}
	}

	public List<E> listAll() throws ServiceException {
		List<E> l = null;
		try {
			l = getDAO().listAll();
			l = afterSearchRecord(l);
		} catch (Exception e) {
			throw new ServiceException("Erro ao listar todos os registros ", e);
		}
		return l;
	}

	public List<E> listAll(final int firstResult, final int maxResult) throws ServiceException {
		List<E> l = null;
		try {
			l = getDAO().listAll(firstResult, maxResult);
			l = afterSearchRecord(l);
		} catch (Exception e) {
			throw new ServiceException("Erro ao listar todos os registros ", e);
		}
		return l;
	}

	public E getInstance(final PK primaryKey) throws ServiceException {
		E ent = null;
		try {
			ent = (E) getDAO().getInstance(primaryKey);
		} catch (Exception e) {
			throw new ServiceException("Erro ao retornar instancia de registro ", e);
		}
		return ent;
	}

	public List<E> findByCriteria(Criterion... criterion) throws ServiceException {
		List<E> l = null;
		try {
			l = getDAO().findByCriteria(criterion);
			l = afterSearchRecord(l);
		} catch (Exception e) {
			throw new ServiceException("Erro ao listar registros via criteria.", e);
		}
		return l;
	}

	public List<E> findByExample(E entity) throws ServiceException {
		List<E> l = null;
		try {
			l = getDAO().findByExample(entity);
			l = afterSearchRecord(l);
		} catch (Exception e) {
			throw new ServiceException("Erro ao listar registros via exemplo.", e);
		}
		return l;
	}

	public List<E> findByCriteria(DetachedCriteria criteria) throws ServiceException {
		List<E> l = null;
		try {
			l = getDAO().findByCriteria(criteria);
			l = afterSearchRecord(l);
		} catch (Exception e) {
			throw new ServiceException("Erro ao listar registros via criteria pela interface DetachedCriteria.", e);
		}
		return l;
	}

	public List<E> findByCriteria(int first, int max, DetachedCriteria criteria) throws ServiceException {
		List<E> l = null;
		try {
			l = getDAO().findByCriteria(first, max, criteria);
			l = afterSearchRecord(l);
		} catch (Exception e) {
			throw new ServiceException("Erro ao listar registros via criteria pela interface DetachedCriteria.", e);
		}
		return l;
	}

	public void beforeSaveAndSaveOrUpdate(E entity) throws ServiceException {
		ClassValidator<Serializable> clazzValidator = new ClassValidator(entity.getClass());
		InvalidValue[] invalidValues = clazzValidator.getInvalidValues((Serializable) entity);
		String ex = "";
		if (invalidValues.length > 0) {
			for (InvalidValue iv : invalidValues) {
				ex = ex + iv.getMessage() + " \n" + "\n";
			}
			throw new ServiceException(ex);
		}
	}

	// antes era public
	protected void afterSaveAndSaveOrUpdate(E entity) throws ServiceException {
	}

	protected void beforeSave(E entity) throws ServiceException {
	}

	protected void afterSave(E entity) throws ServiceException {
	}

	protected void afterDelete(E entity) throws ServiceException {
	}

	protected void beforeDelete(E entity) throws ServiceException {
	}
	//

	protected E afterGetRecord(E entity) throws ServiceException {
		List list = new ArrayList();
		list.add(entity);
		//return afterSearchRecord(list).get(0);
		return (E) afterSearchRecord(list).get(0);
	}

	protected List<E> afterSearchRecord(List<E> list) throws ServiceException {
		return list;
	}

	public List<E> consultaPadrao(String... args) throws ServiceException {
		try {
			return getDAO().consultaPadrao(args);
		} catch (DaoException e) {
			throw new ServiceException(e);
		}
	}

	public List<E> consultaPadrao(int first, int max, String... args) throws ServiceException {
		try {
			return getDAO().consultaPadrao(first, max, args);
		} catch (DaoException e) {
			throw new ServiceException(e);
		}
	}

	public List<E> pesquisaPadrao(String... args) throws ServiceException {
		try {
			return getDAO().pesquisaPadrao(args);
		} catch (DaoException e) {
			throw new ServiceException(e);
		}
	}

	public List<E> pesquisaPadrao(int first, int max, String... args) throws ServiceException {
		try {
			return getDAO().pesquisaPadrao(first, max, args);
		} catch (DaoException e) {
			throw new ServiceException(e);
		}
	}
	
	public List<E> listCombobox(String... args) throws ServiceException {
    	try {
			return getDAO().listCombobox(args);
		} catch (DaoException e) {
			throw new ServiceException(e);
		}
    }

	public List listAnotherQuery(DetachedCriteria criteria) throws ServiceException {
		try {
			return getDAO().listAnotherQuery(criteria);
		} catch (Exception e) {
			throw new ServiceException(e);
		}
	}

	public List findByHql(String hql) throws ServiceException {
		return findByHql(hql, null, null);
	}

	public List findByHql(String hql, ResultTransformer rT) throws ServiceException {
		return findByHql(hql, rT, null);
	}

	public List findByHql(String hql, ResultTransformer rT, Map<String, Object> params) throws ServiceException {
		try {
			return getDAO().findByHql(hql, rT, params);
		} catch (Exception e) {
			throw new ServiceException(e);
		}

	}

	public void executeSql(String sql) throws ServiceException {
		try {
			getDAO().executeSql(sql);
		} catch (Exception e) {
			throw new ServiceException(e);
		}
	}

	public List querySql(String sql, String[] tipos) throws ServiceException {
		try {
			return getDAO().querySql(sql, tipos);
		} catch (Exception e) {
			throw new ServiceException(e);
		}
	}

	protected DaoBase getDAO() throws DaoException {
		DaoBase dao = null;
		// System.out.println("getDaoClass(): "+getDaoClass());
		// System.out.println("getDAO sys: "+sys+" user: "+ user);
		if (!Validator.isEmptyOrNull(sys) && !Validator.isEmptyOrNull(user)) {
			// System.out.println("if");
			dao = DAOFactory.getInstance().getDAOWithAuditing(getDaoClass(), sys, user);
		} else {
			// System.out.println("else");
			dao = DAOFactory.getInstance().getDAO(getDaoClass());
		}
		// System.out.println("DAO: "+ dao);
		return dao;
	}

	public void setUsuarioLogado(Serializable usuarioLogado) throws ServiceException {
		this.usuarioLogado = usuarioLogado;
	}

	public Serializable getUsuarioLogado() { // deixei public porque uso esse
												// metodo pra fazer filtro em
												// alguns sistemas
		return this.usuarioLogado;
	}

	protected AbstractServiceLogic getService(Class clazz) throws ServiceException {
		AbstractServiceLogic retorno = null;
		if (!Validator.isEmptyOrNull(sys) && !Validator.isEmptyOrNull(user)) {
			retorno = (AbstractServiceLogic) ServiceLocator.getInstance().getServiceWithAuditing(clazz, sys, user,
					usuarioLogado);
		} else {
			retorno = (AbstractServiceLogic) ServiceLocator.getInstance().getService(clazz, usuarioLogado);
		}
		return retorno;
	}

	public List orderList(List list, String order) {

		List<BeanComparator> sortFields = new ArrayList();
		sortFields.add(new BeanComparator(order));
		ComparatorChain multiSort = new ComparatorChain(sortFields);
		Collections.sort(list, multiSort);

		return list;
	}

}