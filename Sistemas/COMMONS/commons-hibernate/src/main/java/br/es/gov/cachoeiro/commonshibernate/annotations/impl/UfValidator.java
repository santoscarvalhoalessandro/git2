package br.es.gov.cachoeiro.commonshibernate.annotations.impl;


import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.hibernate.validator.Validator;


import br.es.gov.cachoeiro.commonshibernate.annotations.Uf;

public class UfValidator implements Validator<Uf> {

	public void initialize(Uf arg0) {
	}

	public boolean isValid(Object value) {
		if (value == null) {
			return true;
		}
		String valor = String.valueOf(value);
		
		String reg = "^(ac|AC|al|AL|am|AM|ap|AP|ba|BA|ce|CE|df|DF|es|ES|go|GO|ma|MA|mg|MG|ms|MS|" +  
					 "mt|MT|pa|PA|pb|PB|pe|PE|pi|PI|pr|PR|rj|RJ|rn|RN|ro|RO|rr|RR|rs|RS|sc|SC|se|" +
					 "SE|sp|SP|to|TO)$||()";
		
		Pattern pattern = Pattern.compile(reg);
		Matcher match = pattern.matcher(valor);

		return match.matches();
	}

}

