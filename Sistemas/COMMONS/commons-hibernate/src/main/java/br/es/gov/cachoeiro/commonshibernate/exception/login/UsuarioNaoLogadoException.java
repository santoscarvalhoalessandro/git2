package br.es.gov.cachoeiro.commonshibernate.exception.login;

import br.es.gov.cachoeiro.commons.validator.Validator;
import br.es.gov.cachoeiro.commonshibernate.exception.ServiceException;

public class UsuarioNaoLogadoException extends ServiceException {

	private final static String msg = "O usuário não esta logado no sistema.";

	public UsuarioNaoLogadoException() {
		super();
	}

	/**
	 * @param message
	 */
	public UsuarioNaoLogadoException(String message) {
		super(message != null ? message : msg);
	}

	public UsuarioNaoLogadoException(String message, int codigo) {
		super(!Validator.isEmptyOrNull(message) ? message : msg);
		this.codigo = codigo;
	}

	/**
	 * @param cause
	 */
	public UsuarioNaoLogadoException(Throwable cause) {
		super(cause);
	}

	/**
	 * @param message
	 * @param cause
	 */
	public UsuarioNaoLogadoException(String message, Throwable cause) {
		super(!Validator.isEmptyOrNull(message) ? message : msg, cause);
	}

	private int codigo;

	public int getCodigo() {
		return codigo;
	}

	public void setCodigo(int codigo) {
		this.codigo = codigo;
	}

}
