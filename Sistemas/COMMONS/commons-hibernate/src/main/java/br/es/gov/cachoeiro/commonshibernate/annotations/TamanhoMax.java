package br.es.gov.cachoeiro.commonshibernate.annotations;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;


import org.hibernate.validator.ValidatorClass;

import br.es.gov.cachoeiro.commonshibernate.annotations.impl.TamanhoMaxValidator;

@ValidatorClass(TamanhoMaxValidator.class)
@Target({ElementType.FIELD, ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface TamanhoMax {

	int max();
	String field();
	String message() default "{field} Inválido(a). Sequência de caracteres maior que o permitido.";
}