package br.es.gov.cachoeiro.commonshibernate.dao.impl;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Example;
import org.hibernate.mapping.Table;
import org.hibernate.transform.ResultTransformer;

import br.es.gov.cachoeiro.commons.validator.Validator;
import br.es.gov.cachoeiro.commonsauditing.commons.AuditEntity;
import br.es.gov.cachoeiro.commonsauditing.commons.OperacaoEnum;
import br.es.gov.cachoeiro.commonsauditing.exception.AuditingException;
import br.es.gov.cachoeiro.commonsauditing.manager.AuditManager;
import br.es.gov.cachoeiro.commonsauditing.utils.AuditUtils;
import br.es.gov.cachoeiro.commonshibernate.commons.CommonsAnnotation;
import br.es.gov.cachoeiro.commonshibernate.commons.EntityFactory;
import br.es.gov.cachoeiro.commonshibernate.dao.DaoBase;
import br.es.gov.cachoeiro.commonshibernate.exception.DaoException;
import br.es.gov.cachoeiro.commonshibernate.util.HibernateSqlTranslator;
import br.es.gov.cachoeiro.commonshibernate.util.HibernateUtils;
   
public class GenericDao<T, PK> implements DaoBase<T, PK>{
    protected Session session;  
    private final Class classe;  
 	private boolean autoCommit;
 	private SessionFactory sessionFactory;
 	
	private String sys;
	private String user;

	public void setAuditData(String sys, String user) {
		this.sys = sys;
		this.user = user;
		//System.out.println("genericdao Sys: "+sys+"User: "+user);
	}
 	
    public GenericDao(Session session, Class classe) {  
    	if (session == null) {
   	   		this.session = HibernateUtils.currentSession();
   	    } else {
   	    	this.session = session;
   	    }
     	
    	this.sessionFactory = this.session.getSessionFactory();
        this.classe = classe; 
        autoCommit = true;
    } 
         
    public Session getSession(){  
        if (!this.session.isOpen()) {
        	this.session = HibernateUtils.currentSession(sessionFactory);
        }
    	return this.session;  
    }
    
    public T save(T entity) {
    	Transaction t = startTransaction();
        try{
        	getSession().save(entity);            
        	commitTransaction(t);
        }catch(HibernateException e){
            rollbackTransaction(t);
            throw e;
        } finally{
        	if (isAutoCommit()) {
        		getSession().close();
        	}	
        }
        try{
        	//System.out.println("genericdao save saveaudit");
        	saveAudit(entity, OperacaoEnum.INSERT, sys, user);
        } catch(Exception e){
            e.printStackTrace();
        }
        return entity;
    }    
   
    public List<T> listaTudo() {
    	List<T> l = getSession().createCriteria(this.classe).list();
    	getSession().close();
        return l;
    }  
       
 	public boolean isAutoCommit() {
		return autoCommit;
	}

	public void setAutoCommit(boolean autoCommit) throws DaoException {
		if (isAutoCommit() && session == null) {
			throw new DaoException("Sessão não iniciada.");			
		}
		
		this.autoCommit = autoCommit;
	}

	public Transaction startTransaction() {
		Transaction t = null;
		if (isAutoCommit()) {
			session = getSession();
			t = session.beginTransaction();
		}
		return t;
	}
	
	public void commitTransaction(Transaction t) throws IllegalStateException {
		if (isAutoCommit()) {
			t.commit();
    		//getSession().close();
   	   	}		
	}
	
	public void rollbackTransaction(Transaction t) throws IllegalStateException {
		if (isAutoCommit()) {
			t.rollback();
    		//getSession().close();
		}		
	}

	public void commitTransaction() throws IllegalStateException {
		// TODO Auto-generated method stub
	}

	public void delete(T entity) {
        Transaction t = startTransaction();
        try{
        	getSession().delete(entity);
            commitTransaction(t);
        }catch(HibernateException e){
            rollbackTransaction(t);
            throw e;
		}finally{
        	if (isAutoCommit()) {
        		getSession().close();
        	}
        }	
        try{
        	//System.out.println("genericdao delete saveaudit");
        	saveAudit(entity, OperacaoEnum.DELETE, sys, user);
        } catch(Exception e){
            e.printStackTrace();
        }
    }

    public T get(PK primaryKey) {
        Transaction t = startTransaction();
        try{
            return (T)getSession().get(classe, (Serializable) primaryKey);
        }catch(HibernateException e){
            rollbackTransaction(t);
            e.printStackTrace();
            throw e;
        }finally{
        	if (isAutoCommit()) {
        		getSession().close();
        	}
        }
    }

	public T getInstance(PK primaryKey) {
    	if(primaryKey==null) return null;
    	
    	T entity = get(primaryKey);
    	if(entity==null){
    		try{
    			entity = (T) classe.newInstance();
    		}catch(Exception e){
    			throw new RuntimeException(e); 
    		}
    	}
    	if (isAutoCommit()) {
    		getSession().close();
    	}
    	return entity;
	}

	public List<T> listAll() {
        Transaction t = startTransaction();
        Criteria c = null;
        List<T> l = null;
        
        try{
            c = getSession().createCriteria(classe);
            l = c.list();
        }catch(HibernateException e){
            rollbackTransaction(t);
            throw e;
        }finally{
        	if (isAutoCommit()) {
        		getSession().close();
        	}
        }
        return l;
	}

	public List<T> listAll(int firstResult, int maxResult) {
        Transaction t = startTransaction();
        Criteria c = null;
        List<T> l = null;
        
        try{
            c = getSession().createCriteria(classe);
            if(firstResult!=0){
                c.setFirstResult(firstResult);
            }
            if(maxResult!=0){
                c.setMaxResults(maxResult);
            }
            l = c.list();
        }catch(HibernateException e){
            rollbackTransaction(t);
            throw e;
        }finally{
        	if (isAutoCommit()) {
        		getSession().close();
        	}
        }
        return l;
	}

	public T load(PK primaryKey) {
        Transaction t = startTransaction();        
        T entity = null;
        
        try{
        	entity = (T)getSession().load(classe, (Serializable)primaryKey);
        }catch(HibernateException e){
        	rollbackTransaction(t);
            throw e;
        }finally{
        	if (isAutoCommit()) {
        		getSession().close();
        	}
        }
        return entity;
	}

	public T merge(T entity) {
    	Transaction t = startTransaction();
    	try{
    		entity = (T)getSession().merge(entity);
    		commitTransaction(t);
    		//System.out.println("genericdao merge saveaudit");
        	saveAudit(entity, OperacaoEnum.UPDATE, sys, user);
    	}catch(HibernateException e){
    		rollbackTransaction(t);
    		throw e;
    	} catch (AuditingException e) {
			e.printStackTrace();
		}finally{
        	if (isAutoCommit()) {
        		getSession().close();
        	}
    	}
		return entity;
	}

	public T persist(T entity) {
    	Transaction t = startTransaction();
    	try{
    		getSession().persist(entity);
    		commitTransaction(t);
    	}catch(HibernateException e){
    		rollbackTransaction(t);
    		throw e;
    	} finally{
        	if (isAutoCommit()) {
        		getSession().close();
        	}
    	}
    	
        try{
        	//System.out.println("genericdao persist saveaudit");
        	saveAudit(entity, OperacaoEnum.UPDATE, sys, user);
        } catch(Exception e){
            e.printStackTrace();
        }
		return entity;
	}

	public void refresh(T entity) {
		getSession().refresh(entity);
	}

	public void rollbackTransaction() throws IllegalStateException {
		// TODO Auto-generated method stub
		
	}

	public T saveOrUpdate(T entity) throws DaoException {
        Transaction t = startTransaction();
    	OperacaoEnum operacao = null;
        try{
        	CommonsAnnotation c = new CommonsAnnotation<T>();
//        	if (c.getIdValue(entity) == null) {
//        		operacao = OperacaoEnum.INSERT;
//        	} else {
//        		operacao = OperacaoEnum.UPDATE;
//        	}
        	
        	operacao= OperacaoEnum.UPDATE;
        	
        	getSession().saveOrUpdate(entity);
        	commitTransaction(t);
        }catch(Exception e){
        	rollbackTransaction(t);
            e.printStackTrace();
            throw new DaoException("Erro no metodo saveOrUpdate", e);
        }finally{
        	if (isAutoCommit()) {
        		getSession().close();
        	}
        }
        try{
        	//System.out.println("genericdao saveorupdate saveaudit " + sys + " " + user);
        	saveAudit(entity, operacao, sys, user);
        } catch(Exception e){
            e.printStackTrace();
            throw new DaoException("Erro ao salvar auditoria", e);
        }
        return entity;
	}

	public void update(T entity) {
        Transaction t = startTransaction();
        try{
        	getSession().update(entity);
        	commitTransaction(t);
        	//System.out.println("genericdao update saveaudit " + sys + " " + user);
        	saveAudit(entity, OperacaoEnum.UPDATE, sys, user);
        }catch(HibernateException e){
        	rollbackTransaction(t);
            e.printStackTrace();
            throw e;
        } catch (AuditingException e) {
			e.printStackTrace();
		}finally{
        	if (isAutoCommit()) {
        		getSession().close();
        	}
        }
	}

	public List<T> findByCriteria(Criterion... criterion) {
		List<T> results = null;
		
    	Criteria crit = getSession().createCriteria(classe, "entidade");  
		/*try {
	    	saveAudit(null, OperacaoEnum.SELECT, sys, user, crit.toString());	    	
		} catch (Exception e) {
			e.printStackTrace();
		}*/
   	for (Criterion c : criterion) {  
    		crit.add(c);  
    	}  
    	results = crit.list();
    	if (isAutoCommit()) {
    		getSession().close();
    	}
    	
    	return results;  
    }

	public List<T> findByExample(T entity) {
		List<T> results = null;

        Table table = HibernateUtils.getTable(entity.getClass());
        Criteria crit = getSession().createCriteria(entity.getClass(),table.getName()).add(Example.create(entity));
		/*try {
	    	saveAudit(null, OperacaoEnum.SELECT, sys, user, crit.toString());	    	
		} catch (Exception e) {
			e.printStackTrace();
		}*/
        
		results = crit.list();
		
    	if (isAutoCommit()) {
    		getSession().close();
    	}

    	return results;
	}

	@Override
	public List<T> findByCriteria(DetachedCriteria criteria) {
		List<T> results = null;

		results = criteria.getExecutableCriteria(getSession()).list();
		
    	if (isAutoCommit()) {
    		getSession().close();
    	}
		return results; 
	}	
	public List<T> findByCriteria(int first, int max, DetachedCriteria criteria) {
		List<T> results = null;
		
		results = criteria.getExecutableCriteria(getSession())
				.setFirstResult(first)
				.setMaxResults(max)
				.list();
		
    	if (isAutoCommit()) {
    		getSession().close();
    	}
		return results; 
	}	
	
	public List listAnotherQuery(DetachedCriteria criteria) {
		List results = null;
		
		/*try {
	    	saveAudit(null, OperacaoEnum.SELECT, sys, user, criteria.toString());	    	
		} catch (Exception e) {
			e.printStackTrace();
		}*/
		results = criteria.getExecutableCriteria(getSession()).list();
		
    	if (isAutoCommit()) {
    		getSession().close();
    	}
		return results; 
	}
	
    public List findByHql(String hql) throws DaoException {
		return findByHql(hql, null, null); 
    }

    public List findByHql(String hql, ResultTransformer rT) throws DaoException {
		return findByHql(hql, rT, null); 
    }

    public List findByHql(String hql, ResultTransformer rT, Map<String, Object> params) throws DaoException {
    	List results = null;
		try {
	    	HibernateSqlTranslator translator = new HibernateSqlTranslator();
	    	translator.setSessionFactory(getSession().getSessionFactory());
	    	String sql = translator.toSql(hql);
			
	    	//saveAudit(null, OperacaoEnum.SELECT, sys, user, sql);	    	

	    	Query query = getSession().createQuery(hql);
	    	if (rT != null) {
	    		query.setResultTransformer(rT);
	    	}
	    	if (params != null) {
	    		Set<String> set = params.keySet();
	    		
	    		for (String string : set) {
	    			query.setParameter(string, params.get(string));
				}
	    	}
	    	
			results = query.list();
		
			if (isAutoCommit()) {
				getSession().close();
			}
		} catch (Exception e) {
			throw new DaoException(e.getMessage());
		}
		return results; 
    }
    
    
    public void executeSql(String sql) throws DaoException {
        Transaction t = startTransaction();
        try{
	    	//saveAudit(null, OperacaoEnum.SELECT, sys, user, sql);	    	
        	getSession().createSQLQuery(sql).executeUpdate();
        	commitTransaction(t);
        }catch(HibernateException e){
        	rollbackTransaction(t);
            e.printStackTrace();
            throw e;
		/*} catch (AuditingException e) {
			e.printStackTrace();
		*/}finally{
        	if (isAutoCommit()) {
        		getSession().close();
        	}
        }
    }	
    
    public void executeHql(T entity, OperacaoEnum op, String hql) throws DaoException {
        Transaction t = startTransaction();
        try{	    		    
        	getSession().createQuery(hql).executeUpdate();
        	commitTransaction(t);
        	if(op == OperacaoEnum.INSERT || op == OperacaoEnum.UPDATE || op == OperacaoEnum.DELETE) {
        		saveAudit(entity, op, sys, user, hql);
        	}        		
        }catch(HibernateException e){
        	rollbackTransaction(t);
            e.printStackTrace();
            throw e;
		} catch (AuditingException e) {
			e.printStackTrace();
		}finally{
        	if (isAutoCommit()) {
        		getSession().close();
        	}
        }
    }        

    public List querySql(String sql, String [] tipos) throws DaoException {
        Transaction t = startTransaction();
        List list = null;
        try{
	    	//saveAudit(null, OperacaoEnum.SELECT, sys, user, sql);	    	
        	SQLQuery sqlQuery = getSession().createSQLQuery(sql);
        	for (String str : tipos) {
            	sqlQuery.addScalar(str);
			}
        	list = sqlQuery.list();
        	commitTransaction(t);
        }catch(HibernateException e){
        	rollbackTransaction(t);
            e.printStackTrace();
            throw e;
		/*} catch (AuditingException e) {
			e.printStackTrace();
		*/}finally{
        	if (isAutoCommit()) {
        		getSession().close();
        	}
        }
        return list;
    }	
    
    private void saveAudit(T entity, OperacaoEnum op, String sys, String user, String msg) throws AuditingException {
    	//System.out.println("genericdao saveaudit " + entity + " " + op + " " + sys + " " + user + " " + msg);
    	
		if (entity == null) {
			try {
				entity = (T) EntityFactory.getInstance().getEntity(this.classe);
		    	//System.out.println("entity == null saveAudit: "+entity);
			} catch (Exception e) {
				throw new AuditingException(e);				
			}
		}

		if (!Validator.isEmptyOrNull(sys) && !Validator.isEmptyOrNull(user)) {
			//if (isAutoCommit()) { getSession().close(); }
			//System.out.println("if sys user != null " + sys + " " + user);
			
			if (AuditUtils.auditClass(entity.getClass())) {
				//System.out.println("entity.getclass " + entity.getClass());
				
				AuditEntity aEntity = new AuditEntity();
				aEntity.setEntity((Serializable) entity);
				aEntity.setOp(op);
				aEntity.setSys(sys);
				aEntity.setUser(user);
				aEntity.setMsg(msg);
				aEntity.setSavedfields(Validator.isEmptyOrNull(msg));
				AuditManager.saveAudit(aEntity);
			}
			//if (isAutoCommit()) { getSession().close(); }	    	
		}
	}
	
	private void saveAudit(T entity, OperacaoEnum op, String sys, String user) throws AuditingException {
		saveAudit(entity, op, sys, user, null);		
	}
	
    
    /**
     * ATENCAO, este método será usado para as telas de consulta, usadas no vinculado
     * a consulta deve ser SELECT COLUMN FROM TABLE
     */
    public List consultaPadrao(String... args) throws DaoException {
    	return null;
    }

    /**
     * ATENCAO, este método será usado para as telas de consulta, usadas no vinculado
     * a consulta deve ser SELECT COLUMN FROM TABLE
     */
    public List consultaPadrao(int first, int max, String... args) throws DaoException {
    	return null;
    }
    
    /**
     * ATENCAO, este método será usado nas telas de listagem dos objetos
     * a pesquisa deve ser SELECT * FROM TABLE
     */
    public List pesquisaPadrao(String... args) throws DaoException{
    	return null;
    }
    
    /**
     * ATENCAO, este método será usado nas telas de listagem dos objetos
     * a pesquisa deve ser SELECT * FROM TABLE
     */
    public List pesquisaPadrao(int first, int max, String... args) throws DaoException{
    	return null;
    }
    
    public List listCombobox(String... args) throws DaoException {
    	return null;
    }
    
    /**
     * ATENCAO,
     * este metodo será utilizado pra fazer o Java chamar as procedures do Oracle
     * TODO o metodo que utilizar o getConnection deverá implementar o fechamento das connection  
     */
    public Connection getConection(String user, String password) throws ClassNotFoundException, SQLException {
		
		Class.forName("oracle.jdbc.OracleDriver");
		Connection c = session.connection(); // getSession().connection(); // estava dando erro de connection closed        
		String[] environment = c.getMetaData().getURL().split("@");                        
		//String user = c.getMetaData().getUserName().toLowerCase();
		//String password = c.getMetaData().getUserName().toLowerCase(); // se usuario se senha forem diferentes precisa passar na mao a senha
		c.close(); // fechando conexao
		
		//System.out.println(environment[0] + user + "/" + password + "@" + environment[1]);        			
		//Connection connection = DriverManager.getConnection("jdbc:oracle:thin:USER/PASSWORD@IP_SERVER:PORT:SCHEMA");
		Connection connection = DriverManager.getConnection(environment[0] + user + "/" + password + "@" + environment[1]);
		
		return connection;
	}    
	
 } 