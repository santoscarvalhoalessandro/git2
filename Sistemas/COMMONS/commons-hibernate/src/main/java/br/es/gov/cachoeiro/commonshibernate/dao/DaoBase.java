package br.es.gov.cachoeiro.commonshibernate.dao;


import java.util.List;
import java.util.Map;

import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.transform.ResultTransformer;

import br.es.gov.cachoeiro.commonshibernate.exception.DaoException;

public interface DaoBase <T, PK> {
    
	void setAuditData(String sys, String user);
	
	public T save(T entity);
    public void update(final T entity);
    public T saveOrUpdate(T entity) throws DaoException;
    public void refresh(final T entity);
    public void delete(final T entity);
    public T merge(final T entity);
    public T persist(final T entity);
    public T load(final PK primaryKey);
    public T get(final PK primaryKey);
    public List<T> listAll();
    public List<T> listAll(final int firstResult, final int maxResult);
    public T getInstance(PK primaryKey);
    
    public Transaction  startTransaction();	
    public void commitTransaction() throws IllegalStateException;    
    public void rollbackTransaction() throws IllegalStateException;
    public void commitTransaction(Transaction t) throws IllegalStateException;
    public void rollbackTransaction(Transaction t) throws IllegalStateException;
    
    public boolean isAutoCommit();
    void setAutoCommit(boolean autoCommit)  throws DaoException;
    Session getSession();
    
    List<T> findByCriteria(Criterion... criterion);
    List<T> findByCriteria(DetachedCriteria criteria);
    List<T> findByCriteria(int first, int max, DetachedCriteria criteria);
    List<T> findByExample(T entity);
    List listAnotherQuery(DetachedCriteria criteria);
    List findByHql(String hql) throws DaoException;
    List findByHql(String hql, ResultTransformer rT) throws DaoException;
    List findByHql(String hql, ResultTransformer rT, Map<String, Object> params) throws DaoException; 
    
    List consultaPadrao(String... args) throws DaoException;
    List<T> consultaPadrao(int first, int max, String... args) throws DaoException;
    
    List pesquisaPadrao(String... args) throws DaoException;
    List<T> pesquisaPadrao(int first, int max, String... args) throws DaoException;
    
    List listCombobox(String... args) throws DaoException;
    
    void executeSql(String sql) throws DaoException;
    List querySql(String sql, String [] tipos) throws DaoException;
    
    //Connection getConection() throws ClassNotFoundException, SQLException;
    
}

