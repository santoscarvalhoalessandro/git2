package br.es.gov.cachoeiro.commonshibernate.util;


import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Date;
import java.util.List;

import javax.persistence.Basic;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import br.es.gov.cachoeiro.commonshibernate.exception.ServiceException;

public class HibernateValidator {

	public static void validateRequiredField(Object entity) throws ServiceException {
		validateRequiredField(entity, null);
	}

	public static void validateRequiredField(Object entity, Class clazz) throws ServiceException {
		String msg = "Existem campos obrigatórios que não foram preenchidos!";
		try {
			Class classe = entity.getClass();
			Field[] fields = classe.getDeclaredFields();
			// System.out.println("Nome da Classe multiempresa: " +
			// clazz.getName());
			for (Field field : fields) {
				// System.out.println("Atributo: " + field.getType().getName());
				// System.out.println("Teste do Atributo:
				// "+field.getType().getName().equals(clazz.getName()));

				// if (((clazz != null &&
				// !field.getType().getName().equals(clazz.getName())) || clazz
				// == null) &&
				// field.isAnnotationPresent(Basic.class) &&
				// !field.isAnnotationPresent(Id.class)) {
				if (field.isAnnotationPresent(Basic.class) && !field.isAnnotationPresent(Id.class)) {
					Basic basic = field.getAnnotation(Basic.class);
					if ((!basic.optional()) && br.es.gov.cachoeiro.commons.validator.Validator
							.isEmptyOrNull(getValue(classe, entity, field))) {
						System.out.println(msg + " \nCampo: " + field.getName());
						throw new ServiceException(msg);
					}
				} else if ((field.isAnnotationPresent(ManyToOne.class))
						&& ((clazz != null && !field.getType().getName().equals(clazz.getName())) || clazz == null)) {
					ManyToOne manyToOne = field.getAnnotation(ManyToOne.class);
					if ((!manyToOne.optional()) && br.es.gov.cachoeiro.commons.validator.Validator
							.isEmptyOrNull(getValue(classe, entity, field))) {
						System.out.println(msg + " \nCampo: " + field.getName());
						throw new ServiceException(msg);
					}
				} else {
					// System.out.println("Atributo Entidade do else: " +
					// field.getType().getName());
				}
			}
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (SecurityException e) {
			e.printStackTrace();
		} catch (NoSuchMethodException e) {
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			e.printStackTrace();
		}
	}

	public static void validateOneToManyUsed(Object entity) throws ServiceException {
		String msg = "Este registro esta sendo utilizado para compor outro registro!";
		try {
			Class classe = entity.getClass();
			Field[] fields = classe.getDeclaredFields();

			for (Field field : fields) {
				if (field.isAnnotationPresent(OneToMany.class)) {
					List list = getValueList(classe, entity, field);

					if (list != null && list.size() > 0) {
						throw new ServiceException(msg);
					}
				}
			}
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (SecurityException e) {
			e.printStackTrace();
		} catch (NoSuchMethodException e) {
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			e.printStackTrace();
		}

	}

	private static String getValue(Class clazz, Object object, Field field) throws SecurityException,
			NoSuchMethodException, IllegalArgumentException, IllegalAccessException, InvocationTargetException {
		String methodGet = "get" + field.getName().substring(0, 1).toUpperCase() + field.getName().substring(1);

		Method meth = clazz.getMethod(methodGet);

		Object retobj = meth.invoke(object);

		String retval = null;
		if (retobj == null) {
			retval = "";
		} else if (meth.getReturnType() == Integer.TYPE) {
			retval = ((Integer) retobj).toString();
		} else if (meth.getReturnType() == String.class) {
			retval = (String) retobj;
		} else if (meth.getReturnType() == Double.TYPE) {
			retval = ((Double) retobj).toString();
		} else if (meth.getReturnType() == Date.class) {
			retval = ((Date) retobj).toString();
		} else if (meth.getReturnType() == Character.TYPE) {
			retval = ((Character) retobj).toString();
		} else if (meth.getReturnType() == Short.TYPE) {
			retval = ((Short) retobj).toString();
		} else if (retobj != null) {
			retval = retobj.toString();
		}

		// String retval = (String) retobj;
		// System.out.println(retval);

		return retval;
	}

	private static List getValueList(Class clazz, Object object, Field field) throws SecurityException,
			NoSuchMethodException, IllegalArgumentException, IllegalAccessException, InvocationTargetException {

		String methodGet = "get" + field.getName().substring(0, 1).toUpperCase() + field.getName().substring(1);

		Method meth = clazz.getMethod(methodGet);
		Object retobj = meth.invoke(object);

		List retval = null;
		if (retobj == List.class) {
			retval = (List) retobj;
		}

		return retval;
	}

}
