package br.es.gov.cachoeiro.commonshibernate.commons;

import java.io.Serializable;
import java.lang.reflect.Constructor;
import java.util.HashMap;
import java.util.Properties;

import org.hibernate.Session;

import br.es.gov.cachoeiro.commonshibernate.entity.Entity;
import br.es.gov.cachoeiro.commonshibernate.exception.DaoException;

public class EntityFactory {

	private static EntityFactory singleton;

	private Properties entitysProperties = null;

	private HashMap<Object, Object> entityCache = null;

	/**
	 * Constructor de EntityFactory.
	 */
	private EntityFactory() {

	}

	public static EntityFactory getInstance() {
		if (singleton == null) {
			singleton = new EntityFactory();
		}
		return singleton;
	}

	public Serializable getEntity(Class entityClass, ClassLoader classLoader) throws Exception {
		String name = null;
		Serializable entity = null;

		name = entityClass.getName();
		// System.out.println("getEntity: "+ name);
		try {
			if (classLoader == null) {
				entity = (Serializable) Class.forName(name).newInstance();
			} else {
				Class cl = classLoader.loadClass(name);
				entity = (Serializable) cl.newInstance();
			}
			// dao.setConnection(connection);
		} catch (ClassNotFoundException e) {
			throw new DaoException("A Entidade " + name + " não foi encontrada.", e);
		} catch (Exception e) {
			throw new DaoException("Erro ao carregar a Entidade " + name, e);
		}
		return entity;
	}

	/*
	 * Recupera a dao correspondente e interface fornecida atribuindo uma
	 * conexao e esta;
	 */
	public Serializable getEntity(Class entityClass) throws Exception {
		Serializable entity = getEntity(entityClass, (ClassLoader) null);
		return entity;
	}

	public Serializable getEntity(Class entityClass, Session s) throws Exception {
		Serializable entity = getEntity(entityClass, (ClassLoader) null);
		return entity;
	}

	/*
	 * Recupera a dao correspondente e interface fornecida atribuindo uma
	 * conexao e esta;
	 */
	/*
	 * public BaseDao getDAO(Class daoInterface, Connection conn) throws
	 * DAOException { BaseDao dao = getDAO(daoInterface, (ClassLoader)null);
	 * if(dao != null) { dao.setConnection(conn); } return dao; }
	 */

	/*
	 * Recupera a dao correspondente e interface fornecida atribuindo uma
	 * conexao e esta;
	 */
	/*
	 * public BaseDao getDAO(Class daoInterface, Connection conn, ClassLoader
	 * classLoader) throws DAOException { BaseDao dao = getDAO(daoInterface,
	 * classLoader); if(dao != null) { dao.setConnection(conn); } return dao; }
	 */

	public Serializable getEntityPassingSession(Class entityClass, Session s) throws DaoException {
		Serializable entity = null;
		String name = null;

		Class eClass = null;
		Constructor constructor = null;

		name = entityClass.getName();

		try {
			eClass = Class.forName(name);

			constructor = eClass.getConstructor(new Class[] { Session.class });
			entity = (Entity) constructor.newInstance(new Object[] { s });
		} catch (ClassNotFoundException e) {
			throw new DaoException("A Entidade " + name + " não foi encontrada.", e);
		} catch (Exception e) {
			throw new DaoException("Erro ao carregar a Entidade " + name, e);
		}
		return entity;
	}
}
