package br.es.gov.cachoeiro.commonshibernate.commons;


import java.io.Serializable;
import java.lang.reflect.Method;
import java.util.Properties;

import org.apache.log4j.Logger;

import br.es.gov.cachoeiro.commonshibernate.exception.ServiceException;

/**
 * Permite ao cliente localizar os servicos existentes na aplicacao, em
 * geral existem menos servicos do que casos de uso, enfim este objeto
 * implementa o j2ee design pattern ServiceLocator, veja na pagina as
 * motivacoes e os beneficios em utiliza-lo:
 *
 * http://java.sun.com/blueprints/corej2eepatterns/Patterns/ServiceLocator.html
 *
 */
public class ServiceLocator {

	private static ServiceLocator singleton;
    private static final Logger log = Logger.getLogger(ServiceLocator.class);
    //private HashMap<Object, Object> serviceCache = null;
    private Properties serviceProperties = null;
    
    private ServiceLocator() throws ServiceException {
        //serviceCache = new HashMap<Object, Object>();
    }
    
    /**
     * Recupera a instancia unica de ServiceLocator implementa pattern singleton
     *
     * @return
     * @throws ServiceException
     */
    public static ServiceLocator getInstance()
    throws ServiceException {
        if (singleton == null) {
            singleton = new ServiceLocator();
        }
        return singleton;
    }
    
    /**
     * This is the method that returns the service
     *
     * @param serviceInterface
     * @return servico
     * @throws ServiceException
     */
    public Object getService(Class serviceInterface) throws ServiceException {
    	Object servico = null;
        //if (!serviceCache.containsKey(serviceInterface)) {
            String nome = serviceInterface.getName();
            
            String className = null;
            
            if(className==null) {
                className = serviceInterface.getPackage().getName() + ".impl."+serviceInterface.getSimpleName()+"Impl";
            }
            
            try {
                servico = Class.forName(className).newInstance();
                //serviceCache.put(serviceInterface, servico);
            } catch (InstantiationException e) {
                throw new ServiceException("o servico " + nome + " nao pode ser uma interface ou uma classe abstrata", e.getCause());
            } catch (IllegalAccessException e) {
                throw new ServiceException("o servico " + nome + " esta bloqueado", e.getCause());
            } catch (ClassNotFoundException e) {
                throw new ServiceException("o servico " + nome + " nao foi localizado. "+className, e.getCause());
            }
        //}
        return servico;
        //return serviceCache.get(serviceInterface);
    }
    
    public Object getServiceWithAuditing(Class serviceInterface, String sys, String user) throws ServiceException {
    	Object obj = getService(serviceInterface);

    	Method meth;
		try {
			meth = obj.getClass().getMethod("setAuditData",	new Class[] {String.class, String.class});
			Object retobj = meth.invoke(obj, new Object[] {sys, user});
		} catch (Exception e) {
			e.printStackTrace();
		}    	
    	
    	return obj;    	
    }

    public Object getServiceWithAuditing(Class serviceInterface, String sys, String user, Serializable usuario) throws ServiceException {
    	Object obj = getServiceWithAuditing(serviceInterface, sys, user);
    	return setUsuarioLogado(obj, usuario);    	
    }
    
    public Object getService(Class serviceInterface, Serializable usuario) throws ServiceException {
    	Object obj = getService(serviceInterface);
    	return setUsuarioLogado(obj, usuario);    	
    }  
    
    private Object setUsuarioLogado(Object obj, Serializable usuario) {
    	Method meth;
		try {
			meth = obj.getClass().getMethod("setUsuarioLogado",	new Class[] {Serializable.class});
			Object retobj = meth.invoke(obj, new Object[] {usuario});
		} catch (Exception e) {
			e.printStackTrace();
		}
		return obj;
    }
    
}

