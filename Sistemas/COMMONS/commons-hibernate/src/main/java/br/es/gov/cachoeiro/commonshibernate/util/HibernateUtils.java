package br.es.gov.cachoeiro.commonshibernate.util;


import java.sql.SQLException;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.annotations.common.Version;
import org.hibernate.cfg.AnnotationConfiguration; // hibernate 3
//import org.hibernate.cfg.Configuration; // hibernate 4.3.11
//import org.hibernate.engine.jdbc.connections.spi.ConnectionProvider;
import org.hibernate.mapping.Table;

import br.es.gov.cachoeiro.commons.application.Application;

/**
 * 
 * @author bmxavier
 */
public class HibernateUtils {

	private static final String TRACE_SESSION = "ALTER SESSION SET SQL_TRACE = TRUE";
	
	protected static final ThreadLocal session = new ThreadLocal();
	private static final ThreadLocal sessionCorporativo = new ThreadLocal();
	private static final ThreadLocal sessionAuditing = new ThreadLocal();
	
	private static SessionFactory sessionFactory;
	private static AnnotationConfiguration cfg;

	private static final SessionFactory sessionFactoryCorporativo;
	private static AnnotationConfiguration cfgCorporativo;

	private static final SessionFactory sessionFactoryAuditing;
	private static AnnotationConfiguration cfgAuditing;

	static {
		try {
			if (Application.isValidFile("hibernate.cfg.xml")) {
				System.err.println("Carregando hibernate.cfg.xml.");
				cfg = new AnnotationConfiguration();
				cfg.configure();
				sessionFactory = cfg.buildSessionFactory();
				//System.out.println(sessionFactory.getCollectionMetadata("DataSourceName"));
				//System.out.println(sessionFactory.getAllCollectionMetadata());
			} else {
				cfg = null;
				sessionFactory = null;
				System.err.println("hibernate.cfg.xml não encontrado ");
			}
			
			if (Application.isValidFile("hibernate.corporativo.cfg.xml")) {
				System.err.println("Carregando hibernate.corporativo.cfg.xml.");
				cfgCorporativo = new AnnotationConfiguration();
				cfgCorporativo.configure("hibernate.corporativo.cfg.xml");
				sessionFactoryCorporativo = cfgCorporativo.buildSessionFactory();
				//System.out.println(sessionFactoryCorporativo.getCollectionMetadata("DataSourceName"));
				//System.out.println(sessionFactoryCorporativo.getAllCollectionMetadata());
			} else {
				cfgCorporativo = null;
				sessionFactoryCorporativo = null;
				System.err.println("hibernate.corporativo.cfg.xml não encontrado ");
			}

			if (Application.isValidFile("hibernate.auditing.cfg.xml")) {
				System.err.println("Carregando hibernate.auditing.cfg.xml.");
				cfgAuditing = new AnnotationConfiguration();
				cfgAuditing.configure("hibernate.auditing.cfg.xml");
				sessionFactoryAuditing = cfgAuditing.buildSessionFactory();
				//System.out.println(sessionFactoryAuditing.getCollectionMetadata("DataSourceName"));
				//System.out.println(sessionFactoryAuditing.getAllCollectionMetadata());
			} else {
				cfgAuditing = null;
				sessionFactoryAuditing = null;
				System.err.println("hibernate.auditing.cfg.xml não encontrado ");
			}
		} catch (Throwable ex) {
			System.err.println("Initial SessionFactory creation failed. " + ex);
			throw new ExceptionInInitializerError(ex);
		}
	}
	
	public static String getEnvironment() throws HibernateException, SQLException{
		
		//System.out.println("getenvironment");
		/* 
		 * como este metodo esta depreciado 
		 * pode-se troca-lo por metodo 
		 * que retorne o valor de uma determinada tag do hibernate.cfg
		 *  
		 */
					
		// hibernate 3
		return HibernateUtils.currentSession().connection().getMetaData().getURL();

		// hibernate 4.3.11
		//return HibernateUtils.currentSession().getSessionFactory().getSessionFactoryOptions().getServiceRegistry().getService(ConnectionProvider.class).getConnection().getMetaData().getURL();
		
	}

	public static Session currentSession() throws HibernateException {
		//System.out.println("currentsession");
		Session s = (Session) session.get();
		if (s != null && s.isOpen()){
			//System.out.println("currentsession s!=null && s.isOpen");
			s.close();
		}
		
		if (s == null || !s.isOpen()) {
			//System.out.println("currentsession s==null || !s.isOpen");
			
			if (sessionFactory != null) {
				//System.out.println("currentsession sessionfactory != null");
				s = sessionFactory.openSession();
			} else if (sessionFactoryCorporativo != null) {
				//System.out.println("currentsession sessionfactoryCorporativo != null");
				s = sessionFactoryCorporativo.openSession();
			}	
    		session.set(s);    	
		}
		return s;
	}

	public static Session currentSession(SessionFactory sF) throws HibernateException {				
		//System.out.println("currentsession(sessionfactory)");
		Session s = (Session) session.get();
		if (s != null && s.isOpen()){
			//System.out.println("currentsession(sessionfactory) s!=null && s.isOpen");
			s.close();
		}

		if (s == null || !s.isOpen()) {
			//System.out.println("currentsession(sessionfactory) s==null || !s.isOpen");
			s = sF.openSession();
			s.createSQLQuery(TRACE_SESSION).executeUpdate();
    		session.set(s);    		    		
		}
		return s;
	}

	
	public static void closeSession() throws HibernateException {
		//System.out.println("closeSession");
		Session s = (Session) session.get();
		session.set(null);
		if (s != null) {
			//System.out.println("s!=null");
			s.close();
		}
	}

	public static Table getTable(Class entityClass) {
		//System.out.println("gettable");
		Table table = null;
		
		if (sessionFactory != null) {
			//System.out.println("gettable sessionFactory != null");
			table = cfg.getClassMapping(entityClass.getName()).getTable();;
		} else if (sessionFactoryCorporativo != null) {
			//System.out.println("gettable sessionFactoryCorporativo != null");
			table = cfgCorporativo.getClassMapping(entityClass.getName()).getTable();
		}	
		return table;
	}
	
	public static Session currentSessionCorporativo() throws HibernateException {
		//System.out.println("currentSessionCorporativo");
		Session s = (Session) sessionCorporativo.get();

		if (s == null || !s.isOpen()) {
			//System.out.println("currentSessionCorporativo s==null || !s.isOpen");
			s = sessionFactoryCorporativo.openSession();
			s.createSQLQuery(TRACE_SESSION).executeUpdate();
			sessionCorporativo.set(s);									
		}
		return s;
	}

	public static Session currentSessionAuditing() throws HibernateException {
		//System.out.println("currentSessionAuditing");
		Session s = (Session) sessionAuditing.get();

		if (s == null || !s.isOpen()) {
			//System.out.println("currentSessionAuditing s==null || !s.isOpen");
			s = sessionFactoryAuditing.openSession();
			s.createSQLQuery(TRACE_SESSION).executeUpdate();
			sessionCorporativo.set(s);
		}
		return s;
	}

	public static AnnotationConfiguration getCfg() {
		//System.out.println("getCfg");
		return cfg;
	}

	public static void setCfg(AnnotationConfiguration cfg) {
		//System.out.println("setCfg");
		HibernateUtils.cfg = cfg;
	}
	
	public static void main(String[] args) {		
		System.out.println(Version.VERSION);
	}
	
}