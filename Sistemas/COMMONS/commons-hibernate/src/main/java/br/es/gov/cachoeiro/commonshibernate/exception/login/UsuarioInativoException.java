package br.es.gov.cachoeiro.commonshibernate.exception.login;


import br.es.gov.cachoeiro.commons.validator.Validator;
import br.es.gov.cachoeiro.commonshibernate.exception.ServiceException;

public class UsuarioInativoException extends ServiceException {

	private final static String msg = "Este usuário esta inativo.";

	public UsuarioInativoException() {
		super();
	}

	/**
	 * @param message
	 */
	public UsuarioInativoException(String message) {
		super(!Validator.isEmptyOrNull(message) ? message : msg);
	}

	public UsuarioInativoException(String message, int codigo) {
		super(!Validator.isEmptyOrNull(message) ? message : msg);
		this.codigo = codigo;
	}

	/**
	 * @param cause
	 */
	public UsuarioInativoException(Throwable cause) {
		super(cause);
	}

	/**
	 * @param message
	 * @param cause
	 */
	public UsuarioInativoException(String message, Throwable cause) {
		super(!Validator.isEmptyOrNull(message) ? message : msg, cause);
	}

	private int codigo;

	public int getCodigo() {
		return codigo;
	}

	public void setCodigo(int codigo) {
		this.codigo = codigo;
	}

}
