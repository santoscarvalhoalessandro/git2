package br.es.gov.cachoeiro.commonshibernate.annotations.impl;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.hibernate.validator.Validator;


import br.es.gov.cachoeiro.commonshibernate.annotations.CpfOuCnpj;

public class CpfOuCnpjValidator implements Validator<CpfOuCnpj> {

	public void initialize(CpfOuCnpj arg0) {
	}

	public boolean isValid(Object value) {
		if (value == null) {
			return true;
		}
		String valor = String.valueOf(value);
		
		String reg = "^([0-9]{2,3}?\\.[0-9]{3}?\\.[0-9]{3}?\\-[0-9]{2}?)$||" +
    		         "([0-9]{2}[.]?[0-9]{3}[.]?[0-9]{3}[/]?[0-9]{4}-?[0-9]{2}$)||" +
    		         "()";
		
		Pattern pattern = Pattern.compile(reg);
		Matcher match = pattern.matcher(valor);

		return match.matches();
	}

}
