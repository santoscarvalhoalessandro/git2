package br.es.gov.cachoeiro.commonshibernate.annotations;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import org.hibernate.validator.ValidatorClass;


import br.es.gov.cachoeiro.commonshibernate.annotations.impl.HoraValidator;


@ValidatorClass(HoraValidator.class)
@Target({ElementType.FIELD, ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface Hora {
	String message() default "Hora Inválida. A máscara correta é '99:99'.";

}
