package br.es.gov.cachoeiro.commonsauditing.service.impl;


import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

import br.gov.es.dataci.commons.validator.Validator;
import br.gov.es.dataci.commonsauditing.entity.Admovi;
import br.gov.es.dataci.commonsauditing.persistence.AdmoviDao;
import br.gov.es.dataci.commonsauditing.service.AdmoviService;
import br.gov.es.dataci.commonshibernate.commons.DAOFactory;
import br.gov.es.dataci.commonshibernate.exception.DaoException;
import br.gov.es.dataci.commonshibernate.exception.ServiceException;
import br.gov.es.dataci.commonshibernate.service.impl.AbstractServiceLogic;

public class AdmoviServiceImpl extends AbstractServiceLogic<Admovi, Integer> implements AdmoviService {

	@Override
	public Class<AdmoviDao> getDaoClass() {
		return AdmoviDao.class;
	}

	@Override
	public Class<Admovi> getEntityClass() {
		return Admovi.class;
	}

	/**
	 * @Method: consultaPadrao
	 * @Return: List<Admovi>
	 * @params: sys; classe; pk; user; op; dt;
	 * 
	 **/
	@Override
	public List<Admovi> consultaPadrao(String... args) throws ServiceException {
		DetachedCriteria criterio = DetachedCriteria.forClass(Admovi.class, "movi");
		// criterio.createCriteria("movi.adcampCollection", "camp",
		// Criteria.LEFT_JOIN);
		// criterio.setFetchMode("movi.adcampCollection", FetchMode.SELECT);
		criterio.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
		// System.out.println("Consulta Padrao.");

		criterio.add(Restrictions.eq("movi.moviaplicacao", args[0]));
		if (!Validator.isEmptyOrNull(args[1])) {
			criterio.add(Restrictions.like("movi.movitabela", "%" + args[1]));
		}
		if (!Validator.isEmptyOrNull(args[2])) {
			criterio.add(Restrictions.eq("movi.movipk", args[2]));
		}
		if (!Validator.isEmptyOrNull(args[3])) {
			criterio.add(Restrictions.like("movi.userlogin", args[3] + ""));
		}
		if (!Validator.isEmptyOrNull(args[4])) {
			criterio.add(Restrictions.eq("movi.movioperacao", args[4]));
		}
		if (!Validator.isEmptyOrNull(args[5])) {
			try {
				criterio.add(Restrictions.eq("movi.movidata", new SimpleDateFormat("dd/MM/yyyy").parse(args[5])));
			} catch (ParseException e) {
				e.printStackTrace();
				throw new ServiceException("Data invalida", e);
			}
		}

		criterio.addOrder(Order.asc("movi.moviaplicacao"));
		criterio.addOrder(Order.asc("movi.movitabela"));
		criterio.addOrder(Order.asc("movi.movidata"));

		return findByCriteria(criterio);
	}

	/**
	 * @Method: consultaPadrao
	 * @Return: List<Admovi>
	 * @params: sys; classe; pk; user; op; dt; hql;
	 * 
	 **/
	public List<Admovi> montaHQL(String... args) throws ServiceException {
		StringBuilder sb = new StringBuilder();
		sb.append("select movi from Admovi as movi ");
		sb.append(" left join fetch movi.adcampCollection as camp ");
		sb.append(" where movi.moviaplicacao = '" + args[0] + "'");

		if (!Validator.isEmptyOrNull(args[1])) {
			sb.append(" and movi.movitabela like '%" + args[1] + "'");
		}
		if (!Validator.isEmptyOrNull(args[2])) {
			sb.append(" and movi.movipk = " + args[2]);
		}
		if (!Validator.isEmptyOrNull(args[3])) {
			sb.append(" and movi.userlogin = " + args[3]);
		}
		if (!Validator.isEmptyOrNull(args[4])) {
			sb.append(" and movi.movioperacao = " + args[4]);
		}
		if (!Validator.isEmptyOrNull(args[5])) {
			try {
				sb.append("movi.movidata = " + new SimpleDateFormat("dd/MM/yyyy").parse(args[5]));
			} catch (ParseException e) {
				e.printStackTrace();
				throw new ServiceException("Data invalida", e);
			}
		}

		if (!Validator.isEmptyOrNull(args[6])) {
			sb.append(" and ");
			sb.append(args[6]);
		}
		sb.append(" order by movi.moviaplicacao, movi.movitabela, movi.movidata");

		// System.out.println("HQL: "+sb.toString());

		return findByHql(sb.toString(), Criteria.DISTINCT_ROOT_ENTITY);
	}

	public List consultaList(String... args) throws ServiceException {
		List<Admovi> list = consultaPadrao(args);

		return null;
	}

	public List listAllApli() throws ServiceException {

		DetachedCriteria criterio = DetachedCriteria.forClass(Admovi.class);
		criterio.setProjection(Projections.distinct(Projections.property("moviaplicacao")));

		return findByCriteria(criterio);
	}

	//@Override
	public Admovi save(Admovi entity, Session s) throws ServiceException {
		if (s != null) {
			AdmoviDao moviDao = null;
			try {
				moviDao = (AdmoviDao) DAOFactory.getInstance().getDAOPassingSession(AdmoviDao.class, s);
				if (s != null) {
					moviDao.setAutoCommit(false);
				}
			} catch (DaoException d) {
				throw new ServiceException(d);
			}

			return (Admovi) moviDao.save(entity);
		} else {
			return super.save(entity);
		}
	}

}
