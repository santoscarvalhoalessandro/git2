package br.es.gov.cachoeiro.commonsauditing.commons;


import java.io.Serializable;

public class AuditEntity {

	private Serializable entity;
	private String user; 
	private String sys; 
	private OperacaoEnum op;
	private String msg;
	private boolean savedfields = true;
	
	public Serializable getEntity() {
		return entity;
	}

	public void setEntity(Serializable entity) {
		this.entity = entity;
	}

	public String getUser() {
		return user;
	}

	public void setUser(String user) {
		this.user = user;
	}

	public String getSys() {
		return sys;
	}

	public void setSys(String sys) {
		this.sys = sys;
	}

	public OperacaoEnum getOp() {
		return op;
	}

	public void setOp(OperacaoEnum op) {
		this.op = op;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	public boolean isSavedfields() {
		return savedfields;
	}

	public void setSavedfields(boolean savedfields) {
		this.savedfields = savedfields;
	}

}

