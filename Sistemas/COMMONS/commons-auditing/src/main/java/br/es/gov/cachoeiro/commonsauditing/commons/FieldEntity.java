package br.es.gov.cachoeiro.commonsauditing.commons;


public class FieldEntity {

	private String name;
	private String value;
	
	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getValue() {
		return value;
	}


	public void setValue(String value) {
		this.value = value;
	}

}

