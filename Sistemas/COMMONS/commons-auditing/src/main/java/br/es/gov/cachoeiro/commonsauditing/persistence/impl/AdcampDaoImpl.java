package br.es.gov.cachoeiro.commonsauditing.persistence.impl;

import org.hibernate.Session;

import br.gov.es.dataci.commonsauditing.entity.Adcamp;
import br.gov.es.dataci.commonsauditing.persistence.AdcampDao;
import br.gov.es.dataci.commonshibernate.dao.impl.GenericDao;
import br.gov.es.dataci.commonshibernate.util.HibernateUtils;

public class AdcampDaoImpl extends GenericDao<Adcamp, Integer> implements AdcampDao<Adcamp, Integer> {

	public AdcampDaoImpl(Session session) {
		super(session, Adcamp.class);
	}

	public AdcampDaoImpl() {
		super(HibernateUtils.currentSessionAuditing(), Adcamp.class);
	}

}