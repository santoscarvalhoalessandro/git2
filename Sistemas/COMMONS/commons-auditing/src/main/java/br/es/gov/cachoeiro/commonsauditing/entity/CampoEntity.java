package br.es.gov.cachoeiro.commonsauditing.entity;


public class CampoEntity {
	private String campatributo;
	private String campvalor;
	public String getCampatributo() {
		return campatributo;
	}
	public void setCampatributo(String campatributo) {
		this.campatributo = campatributo;
	}
	public String getCampvalor() {
		return campvalor;
	}
	public void setCampvalor(String campvalor) {
		this.campvalor = campvalor;
	}
	public CampoEntity(String campatributo, String campvalor) {
		super();
		this.campatributo = campatributo;
		this.campvalor = campvalor;
	}
	public CampoEntity() {
		super();
	}

}

