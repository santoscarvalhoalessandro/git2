package br.es.gov.cachoeiro.commonsauditing.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import br.gov.es.dataci.commonsauditing.annotation.AuditTable;

/**
 *
 * @author bruno
 */
@Entity
@Table(name = "ADMOVI")
@AuditTable(Auditing=false)
public class Admovi implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @SequenceGenerator(name = "SEQ_ADMOVI_MOVICODIGO", sequenceName = "SEQ_ADMOVI_MOVICODIGO", allocationSize=1) 
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_ADMOVI_MOVICODIGO") 
    @Basic(optional = false)
    @Column(name = "MOVICODIGO", nullable = false)
    private Integer movicodigo;
    @Column(name = "USERLOGIN", length = 80)
    private String userlogin;
    @Column(name = "MOVIDATA")
    @Temporal(TemporalType.TIMESTAMP)
    private Date movidata;
    @Column(name = "MOVITABELA", length = 80)
    private String movitabela;
    @Column(name = "MOVIOPERACAO")
    private String movioperacao;
    @Column(name = "MOVIPK", length = 80)
    private String movipk;
    @Column(name = "MOVIAPLICACAO", length = 80)
    private String moviaplicacao;
    @Lob
    @Column(name = "MOVILOG")
    private String movilog;
    @Lob
    @Column(name = "MOVICAMPOS")
    private String movicampos;
    @Column
    @OneToMany(mappedBy = "movicodigo", fetch = FetchType.EAGER)
    private List<Adcamp> adcampCollection;

    public Admovi() {
    }

    public Admovi(Integer movicodigo) {
        this.movicodigo = movicodigo;
    }

    public Integer getMovicodigo() {
        return movicodigo;
    }

    public void setMovicodigo(Integer movicodigo) {
        this.movicodigo = movicodigo;
    }

    public String getUserlogin() {
        return userlogin;
    }

    public void setUserlogin(String userlogin) {
        this.userlogin = userlogin;
    }

    public Date getMovidata() {
        return movidata;
    }

    public void setMovidata(Date movidata) {
        this.movidata = movidata;
    }

    public String getMovitabela() {
        return movitabela;
    }

    public void setMovitabela(String movitabela) {
        this.movitabela = movitabela;
    }

    public String getMovioperacao() {
        return movioperacao;
    }

    public void setMovioperacao(String movioperacao) {
        this.movioperacao = movioperacao;
    }

    public String getMovipk() {
        return movipk;
    }

    public void setMovipk(String movipk) {
        this.movipk = movipk;
    }

    public String getMoviaplicacao() {
        return moviaplicacao;
    }

    public void setMoviaplicacao(String moviaplicacao) {
        this.moviaplicacao = moviaplicacao;
    }

    public List<Adcamp> getAdcampCollection() {
        return adcampCollection;
    }

    public void setAdcampCollection(List<Adcamp> adcampCollection) {
        this.adcampCollection = adcampCollection;
    }

    public String getMovilog() {
		return movilog;
	}

	public void setMovilog(String movilog) {
		this.movilog = movilog;
	}

	@Override
    public int hashCode() {
        int hash = 0;
        hash += (movicodigo != null ? movicodigo.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Admovi)) {
            return false;
        }
        Admovi other = (Admovi) object;
        if ((this.movicodigo == null && other.movicodigo != null) || (this.movicodigo != null && !this.movicodigo.equals(other.movicodigo))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "org.javafree.ejb.Admovi[movicodigo=" + movicodigo + "]";
    }

	public String getMovicampos() {
		return movicampos;
	}

	public void setMovicampos(String movicampos) {
		this.movicampos = movicampos;
	}

}
