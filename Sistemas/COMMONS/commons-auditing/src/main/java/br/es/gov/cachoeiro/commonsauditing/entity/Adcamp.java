package br.es.gov.cachoeiro.commonsauditing.entity;


import java.io.Serializable;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import br.gov.es.dataci.commonsauditing.annotation.AuditTable;

/**
 *
 * @author bruno
 */
@Entity
@Table(name = "ADCAMP")
@AuditTable(Auditing=false)
public class Adcamp implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @SequenceGenerator(name = "SEQ_ADCAMP_CAMPCODIGO", sequenceName = "SEQ_ADCAMP_CAMPCODIGO", allocationSize=1) 
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_ADCAMP_CAMPCODIGO") 
    @Basic(optional = false)
    @Column(name = "CAMPCODIGO", nullable = false)
    private Integer campcodigo;
    @Column(name = "CAMPATRIBUTO", length = 80)
    private String campatributo;
    @Lob
    @Column(name = "CAMPVALOR")
    private String campvalor;
    @JoinColumn(name = "MOVICODIGO", referencedColumnName = "MOVICODIGO")
    @ManyToOne
    private Admovi movicodigo;

    public Adcamp() {
    }

    public Adcamp(Integer campcodigo) {
        this.campcodigo = campcodigo;
    }

    public Integer getCampcodigo() {
        return campcodigo;
    }

    public void setCampcodigo(Integer campcodigo) {
        this.campcodigo = campcodigo;
    }

    public String getCampatributo() {
        return campatributo;
    }

    public void setCampatributo(String campatributo) {
        this.campatributo = campatributo;
    }

    public String getCampvalor() {
        return campvalor;
    }

    public void setCampvalor(String campvalor) {
        this.campvalor = campvalor;
    }

    public Admovi getMovicodigo() {
        return movicodigo;
    }

    public void setMovicodigo(Admovi movicodigo) {
        this.movicodigo = movicodigo;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (campcodigo != null ? campcodigo.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Adcamp)) {
            return false;
        }
        Adcamp other = (Adcamp) object;
        if ((this.campcodigo == null && other.campcodigo != null) || (this.campcodigo != null && !this.campcodigo.equals(other.campcodigo))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "org.javafree.ejb.Adcamp[campcodigo=" + campcodigo + "]";
    }

}
