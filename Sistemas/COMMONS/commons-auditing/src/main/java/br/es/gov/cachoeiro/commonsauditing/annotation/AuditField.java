package br.es.gov.cachoeiro.commonsauditing.annotation;

public @interface AuditField {
	boolean Auditing() default true;
	String SubstituteText() default "";
	
}
