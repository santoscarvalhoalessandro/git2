package br.es.gov.cachoeiro.commonsauditing.service;

import java.util.List;

import org.hibernate.Session;

import br.gov.es.dataci.commonsauditing.entity.Admovi;
import br.gov.es.dataci.commonshibernate.exception.ServiceException;
import br.gov.es.dataci.commonshibernate.service.ServiceLogic;

public interface AdmoviService extends ServiceLogic<Admovi, Integer> {

	List consultaList(String... args) throws ServiceException;

	List listAllApli() throws ServiceException;

	Admovi save(Admovi entity, Session s) throws ServiceException;

	public List<Admovi> montaHQL(String... args) throws ServiceException;
}
