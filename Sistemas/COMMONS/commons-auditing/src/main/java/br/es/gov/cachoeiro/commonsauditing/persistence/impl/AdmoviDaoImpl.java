package br.es.gov.cachoeiro.commonsauditing.persistence.impl;


import java.util.List;

import org.hibernate.Session;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import br.gov.es.dataci.commons.validator.Validator;
import br.gov.es.dataci.commonsauditing.entity.Admovi;
import br.gov.es.dataci.commonsauditing.persistence.AdmoviDao;
import br.gov.es.dataci.commonshibernate.commons.DAOFactory;
import br.gov.es.dataci.commonshibernate.dao.impl.GenericDao;
import br.gov.es.dataci.commonshibernate.exception.DaoException;
import br.gov.es.dataci.commonshibernate.util.HibernateUtils;

public class AdmoviDaoImpl extends GenericDao<Admovi, Integer> implements AdmoviDao<Admovi, Integer> {

	public AdmoviDaoImpl(Session session) {
		super(session, Admovi.class);
	}

	public AdmoviDaoImpl() {
		super(HibernateUtils.currentSessionAuditing(), Admovi.class);
		// System.out.println("AdmoviDaoImpl");
	}
	
	@Override
	public List consultaPadrao(String... args) throws DaoException {
		DetachedCriteria crit = DetachedCriteria.forClass(Admovi.class, "entidade");
		
		if(args.length > 0 && !Validator.isEmptyNullOrZero(args[0])){
			crit.add(Restrictions.eq("entidade.movicodigo", Integer.parseInt(args[0])));
		}
		
		return findByCriteria(crit);
	}
	
	public static void main(String[] args) throws DaoException {
		AdmoviDao dao = (AdmoviDao) DAOFactory.getInstance().getDAO(AdmoviDao.class);
		System.out.println(dao.consultaPadrao("10"));
	}

}
