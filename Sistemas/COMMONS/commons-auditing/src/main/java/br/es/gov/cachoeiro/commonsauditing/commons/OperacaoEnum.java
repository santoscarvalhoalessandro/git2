package br.es.gov.cachoeiro.commonsauditing.commons;

public enum OperacaoEnum {

	NULL("", ""),
	INSERT("I", "INSERT"),
	UPDATE("U", "UPDATE"),
	DELETE("D", "DELETE"),
	
	SELECT("S", "SELECT"),
	LOGIN("L", "LOGIN"),
	
	DEBUG_INFO("F", "DEBUG INFO"),
	DEBUG_WARNING("W", "DEBUG WARNING"),
	DEBUG_ERROR("E", "DEBUG ERROR");
	
	private String cod;
	private String descr;

	private OperacaoEnum(String cod, String descr) {
		this.cod = cod;		
		this.descr = descr;
	}
	
	
	public String getCod() {
		return cod;
	}

	public void setCod(String cod) {
		this.cod = cod;
	}
	
	public static OperacaoEnum get(String s) {
		for (OperacaoEnum e : values()) {
			if (e.cod.equals(s)) {
				return e;
			}
		}
		return null;
	}


	public String getDescr() {
		return descr;
	}


	public void setDescr(String descr) {
		this.descr = descr;
	}
	
	@Override
	public String toString() {
		return descr;
	}
	
}
