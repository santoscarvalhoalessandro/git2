package br.es.gov.cachoeiro.commonsauditing.service.impl;


import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import br.gov.es.dataci.commons.validator.Validator;
import br.gov.es.dataci.commonsauditing.entity.Adcamp;
import br.gov.es.dataci.commonsauditing.persistence.AdcampDao;
import br.gov.es.dataci.commonsauditing.service.AdcampService;
import br.gov.es.dataci.commonshibernate.commons.DAOFactory;
import br.gov.es.dataci.commonshibernate.exception.DaoException;
import br.gov.es.dataci.commonshibernate.exception.ServiceException;
import br.gov.es.dataci.commonshibernate.service.impl.AbstractServiceLogic;

public class AdcampServiceImpl extends AbstractServiceLogic<Adcamp, Integer> implements AdcampService {

	@Override
	public Class<AdcampDao> getDaoClass() {
		return AdcampDao.class;
	}

	@Override
	public Class<Adcamp> getEntityClass() {
		return Adcamp.class;
	}

	/**
	 * @Method: consultaPadrao
	 * @Return: List<Admovi>
	 * @params: sys; classe; pk; user; op; dt;
	 * 
	 **/
	@Override
	public List<Adcamp> consultaPadrao(String... args) throws ServiceException {
		DetachedCriteria criterio = DetachedCriteria.forClass(Adcamp.class, "camp");
		criterio.createAlias("camp.movicodigo", "movi");

		criterio.add(Restrictions.eq("movi.moviaplicacao", args[0]));
		criterio.add(Restrictions.like("movi.movitabela", "%" + args[1]));
		if (!Validator.isEmptyOrNull(args[2])) {
			criterio.add(Restrictions.eq("movi.movipk", args[2]));
		}
		if (!Validator.isEmptyOrNull(args[3])) {
			criterio.add(Restrictions.like("movi.userlogin", args[3] + ""));
		}
		if (!Validator.isEmptyOrNull(args[4])) {
			criterio.add(Restrictions.eq("movi.movioperacao", args[4]));
		}
		if (!Validator.isEmptyOrNull(args[5])) {
			try {
				criterio.add(Restrictions.eq("movi.movidata", new SimpleDateFormat("dd/MM/yyyy").parse(args[5])));
			} catch (ParseException e) {
				e.printStackTrace();
				throw new ServiceException("Data invalida", e);
			}
		}
		criterio.addOrder(Order.asc("movi.moviaplicacao"));
		criterio.addOrder(Order.asc("movi.movitabela"));
		criterio.addOrder(Order.asc("movi.movidata"));

		return findByCriteria(criterio);
	}

	//@Override
	public Adcamp save(Adcamp entity, Session s) throws ServiceException {
		if (s != null) {
			AdcampDao campDao = null;
			try {
				campDao = (AdcampDao) DAOFactory.getInstance().getDAOPassingSession(AdcampDao.class, s);
				if (s != null) {
					campDao.setAutoCommit(false);
				}
			} catch (DaoException d) {
				throw new ServiceException(d);
			}

			return (Adcamp) campDao.save(entity);
		} else {
			return super.save(entity);
		}
	}

}
