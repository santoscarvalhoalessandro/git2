package br.es.gov.cachoeiro.commonsauditing.annotation;

import java.lang.annotation.Documented;

@Documented
public @interface AuditTable {
	boolean Auditing() default true;
}