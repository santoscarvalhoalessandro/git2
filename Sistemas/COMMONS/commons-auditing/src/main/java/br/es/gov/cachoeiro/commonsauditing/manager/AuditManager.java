package br.es.gov.cachoeiro.commonsauditing.manager;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.EmbeddedId;
import javax.persistence.Id;

import org.hibernate.Session;
import org.hibernate.Transaction;

import br.gov.es.dataci.commons.serializacao.Serializacao;
import br.gov.es.dataci.commonsauditing.commons.AuditEntity;
import br.gov.es.dataci.commonsauditing.commons.FieldEntity;
import br.gov.es.dataci.commonsauditing.entity.Admovi;
import br.gov.es.dataci.commonsauditing.entity.CampoEntity;
import br.gov.es.dataci.commonsauditing.exception.AuditingException;
import br.gov.es.dataci.commonsauditing.service.AdmoviService;
import br.gov.es.dataci.commonshibernate.commons.ServiceLocator;
import br.gov.es.dataci.commonshibernate.util.HibernateUtils;

public class AuditManager {

	private AuditEntity auditEntity;

	public AuditManager(AuditEntity auditEntity) {
		this.auditEntity = auditEntity;
	}

	public static void saveAudit(AuditEntity auditEntity) throws AuditingException {
		criarMovimentacao(auditEntity);
	}

	public void saveAudit() throws AuditingException {
		criarMovimentacao(auditEntity);
	} 

	private static Admovi criarMovimentacao(AuditEntity auditEntity) throws AuditingException {
		Admovi admovi = new Admovi();
		Session session = HibernateUtils.currentSessionAuditing();

		Transaction t = null;
		try {
			FieldReading fR = new FieldReading(auditEntity);
			List<FieldEntity> listFieldEntity = fR.findFieldByAnnotation(Id.class);

			if (listFieldEntity == null || listFieldEntity.isEmpty()) {
				listFieldEntity = fR.findFieldByAnnotation(EmbeddedId.class);
			}

			FieldEntity fE = listFieldEntity.get(0);

			/* Criando uma lista de campos */
			List<FieldEntity> list = fR.getAllFields();
			List<CampoEntity> listCamp = new ArrayList();

			for (FieldEntity fEntity : list) {
				CampoEntity adcamp = new CampoEntity(fEntity.getName(), fEntity.getValue());
				listCamp.add(adcamp);
			}

			t = session.beginTransaction();

			admovi.setMovidata(new Date());
			admovi.setMoviaplicacao(auditEntity.getSys());
			admovi.setMovioperacao(auditEntity.getOp().getCod());
			admovi.setMovitabela(getClassName(auditEntity));
			admovi.setUserlogin(auditEntity.getUser());
			admovi.setMovilog(auditEntity.getMsg());

			admovi.setMovipk(fE.getValue());
			admovi.setMovicampos(Serializacao.serializaXML((Serializable) listCamp));

			AdmoviService moviService;
			moviService = (AdmoviService) ServiceLocator.getInstance().getService(AdmoviService.class);
			admovi = moviService.save(admovi, session);

			/*if (!auditEntity.isSavedfields()) {
				return admovi;
			}*/

			/*
			 * AdcampService campService = (AdcampService)
			 * ServiceLocator.getInstance().getService(AdcampService.class);
			 * List<FieldEntity> list = fR.getAllFields(); for (FieldEntity
			 * fEntity : list) { Adcamp adcamp = new Adcamp();
			 * adcamp.setMovicodigo(admovi);
			 * adcamp.setCampatributo(fEntity.getName());
			 * adcamp.setCampvalor(fEntity.getValue()); campService.save(adcamp,
			 * session); }
			 */
			t.commit();
		} catch (Exception e) {
			t.rollback();
			e.printStackTrace();
			throw new AuditingException(e);
		} finally {
			// System.out.println("Fechando Sessao [criarMovimentacao]");
			if (session.isOpen()) {
				session.close();
			}
		}

		return admovi;
	}

	public AuditEntity getAuditEntity() {
		return auditEntity;
	}

	public void setAuditEntity(AuditEntity auditEntity) {
		this.auditEntity = auditEntity;
	}

	private static String getClassName(AuditEntity auditEntity) {
		return auditEntity.getEntity().getClass().getName();
	}

}
