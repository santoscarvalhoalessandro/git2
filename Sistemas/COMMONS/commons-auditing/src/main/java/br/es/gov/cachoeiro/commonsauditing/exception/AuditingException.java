package br.es.gov.cachoeiro.commonsauditing.exception;


public class AuditingException extends Exception {
    /**
     *
     */
    public AuditingException() {
        super();
    }
    
    /**
     * @param message
     */
    public AuditingException(String message) {
        super(message);
    }
    
     public AuditingException(String message, int codigo) {
        super(message);
        this.codigo = codigo;
    }
    
    /**
     * @param cause
     */
    public AuditingException(Throwable cause) {
        super(cause);
    }
    
    /**
     * @param message
     * @param cause
     */
    public AuditingException(String message, Throwable cause) {
        super(message, cause);
    }
    
     private int codigo;

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }
}
