package br.es.gov.cachoeiro.commonsauditing.manager;


import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

import br.gov.es.dataci.commons.validator.Validator;
import br.gov.es.dataci.commonsauditing.annotation.AuditField;
import br.gov.es.dataci.commonsauditing.commons.AuditEntity;
import br.gov.es.dataci.commonsauditing.commons.FieldEntity;
import br.gov.es.dataci.commonsauditing.utils.AuditUtils;

public class FieldReading {

	private AuditEntity auditEntity;

	public FieldReading(AuditEntity auditEntity) {
		this.auditEntity = auditEntity;
	}

	public List<FieldEntity> findFieldByAnnotation(Class annotationClass) {
		List<FieldEntity> list = new ArrayList();
		try {
			Class classe = auditEntity.getEntity().getClass();
			Field[] fields = classe.getDeclaredFields();
			for (Field field : fields) {
				if (field.isAnnotationPresent(annotationClass)) {
					FieldEntity fE = new FieldEntity();
					fE.setName(field.getName());
					fE.setValue(getValue(classe, auditEntity.getEntity(), field));
					list.add(fE);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return list;
	}

	public List<FieldEntity> getAllFields() {
		List<FieldEntity> list = new ArrayList();
		try {
			Class classe = auditEntity.getEntity().getClass();
			Field[] fields = classe.getDeclaredFields();
			for (Field field : fields) {
				AuditField aField = AuditUtils.auditField(field);
				if (!field.getName().equals("serialVersionUID") && (aField == null || !aField.Auditing())) {
					FieldEntity fE = new FieldEntity();
					fE.setName(field.getName());
					if (aField == null || Validator.isEmptyOrNull(aField.SubstituteText())) {
						fE.setValue(getValue(classe, auditEntity.getEntity(), field));
					} else {
						fE.setValue(aField.SubstituteText());
					}
					list.add(fE);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return list;
	}

	private static String getValue(Class clazz, Object object, Field field) throws Exception {
		String methodGet = "get" + field.getName().substring(0, 1).toUpperCase() + field.getName().substring(1);

		Method meth = clazz.getMethod(methodGet);

		if (meth.getReturnType().getName().contains("List")) {
			return "List";
		}

		Object retobj = meth.invoke(object);

		String retval = null;
		if (retobj == null) {
			retval = "";
		} else {
			retval = retobj.toString();
		}

		return retval;
	}

}