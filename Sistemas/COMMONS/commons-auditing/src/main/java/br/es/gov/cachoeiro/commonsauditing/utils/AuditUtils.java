package br.es.gov.cachoeiro.commonsauditing.utils;


import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

import br.gov.es.dataci.commonsauditing.annotation.AuditField;
import br.gov.es.dataci.commonsauditing.annotation.AuditTable;

public class AuditUtils {

	public static boolean auditClass(Class clazz) {
		boolean result = true;
		if (clazz.isAnnotationPresent(AuditTable.class)) {
			AuditTable a = (AuditTable) clazz.getAnnotation(AuditTable.class);
			result = a.Auditing();
		}

		return result;
	}

	public static List<String> getFieldsClass(Class clazz) {
		List<String> list = new ArrayList();
		try {

			Field[] fields = clazz.getDeclaredFields();
			for (Field field : fields) {
				if (!field.getName().equals("serialVersionUID")) {
					list.add(field.getName());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return list;
	}

	public static AuditField auditField(Field field) {
		AuditField result = null;
		if (field.isAnnotationPresent(AuditField.class)) {
			AuditField a = (AuditField) field.getAnnotation(AuditField.class);
			result = a;
		}

		return result;
	}

}

