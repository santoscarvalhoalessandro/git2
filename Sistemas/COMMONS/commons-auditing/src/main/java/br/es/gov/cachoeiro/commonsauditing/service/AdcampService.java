package br.es.gov.cachoeiro.commonsauditing.service;

import org.hibernate.Session;

import br.gov.es.dataci.commonsauditing.entity.Adcamp;
import br.gov.es.dataci.commonsauditing.entity.Admovi;
import br.gov.es.dataci.commonshibernate.exception.ServiceException;
import br.gov.es.dataci.commonshibernate.service.ServiceLogic;

public interface AdcampService extends ServiceLogic<Adcamp, Integer> {

	Adcamp save(Adcamp entity, Session s) throws ServiceException;

}


