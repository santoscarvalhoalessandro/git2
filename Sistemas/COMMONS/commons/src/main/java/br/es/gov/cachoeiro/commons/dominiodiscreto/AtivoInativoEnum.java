package br.es.gov.cachoeiro.commons.dominiodiscreto;


public enum AtivoInativoEnum {
	
	ATIVO('A', "ATIVO"),
	INATIVO('I', "INATIVO");	
	
    private String descr;
    private char cod;

    public static AtivoInativoEnum get(char codigo) {
        for (AtivoInativoEnum tipo : AtivoInativoEnum.values()) {
            if (tipo.cod == codigo) {
                return tipo;
            }
        }
        return null;
    }

	public String getDescr() {
		return descr;
	}
	public void setDescr(String descr) {
		this.descr = descr;
	}
	public char getCod() {
		return cod;
	}
	public void setCod(char cod) {
		this.cod = cod;
	}

	private AtivoInativoEnum(char cod, String descr) {
		this.descr = descr;
		this.cod = cod;
	}

}
