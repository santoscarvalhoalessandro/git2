package br.es.gov.cachoeiro.commons.enumerator;


public enum SexoEnum {
	MASCULINO('M', "MASCULINO"),
	FEMININO('F', "FEMININO");

    private String descr;
    private char cod;
    
    private SexoEnum(char cod, String descr) {
    	this.cod = cod;
		this.descr = descr;
		
	}
	
	public char getCod() {
		return cod;
	}

	public void setCod(char cod) {
		this.cod = cod;
	}

	public String getDescr() {
		return descr;
	}

	public void setDescr(String descr) {
		this.descr = descr;
	}
	
	public static String get(char i){
		String s = null;
		
		if (Character.valueOf(i).equals(MASCULINO.getCod())) {
			s = MASCULINO.getDescr();
		} else if (Character.valueOf(i).equals(FEMININO.getCod())) {
			s = FEMININO.getDescr();
		} else {
			s ="NAO INFORMADO";
		}
		
		return s;		
	}
	
	public static SexoEnum getEnum(char i){
		SexoEnum s = null;
		
		if (Character.valueOf(i).equals(MASCULINO.getCod())) {
			s = MASCULINO;
		} else if (Character.valueOf(i).equals(FEMININO.getCod())) {
			s = FEMININO;
		} 
		
		return s;		
	}

}


