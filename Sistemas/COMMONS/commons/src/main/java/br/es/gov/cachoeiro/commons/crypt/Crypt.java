package br.es.gov.cachoeiro.commons.crypt;

public class Crypt {

	public static String encryptBase100(String password){
		String crypt = "";

		for (int x=0; x<=password.length()-1; x++) {
			int j = ((int) password.charAt(x)) + 100;
			crypt += Integer.toString(j);
		}

		return crypt;
	}

	public static String decryptBase100(String password){
		String crypt = "";

		for (int x=0; x<=password.length()-1; x+=3) {
			String s = password.substring(x,x+3);
			crypt += ((char)(Integer.valueOf(s)-100));
		}

		return crypt;
		//FERNANDO
	}

}
