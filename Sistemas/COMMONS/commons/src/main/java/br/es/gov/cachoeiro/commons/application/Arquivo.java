package br.es.gov.cachoeiro.commons.application;

public class Arquivo extends ArquivoAbstract {

	public static ArquivoAbstract getInstance() {
		return new Arquivo(); 
		
	}

	@Override
	public String trataLinha(String linha) {
		return linha;
	}

	@Override
	public boolean retiraLinha(String linha) {		
		return false;
	}
	
}
