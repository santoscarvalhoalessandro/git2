package br.es.gov.cachoeiro.commons.validator;

import java.util.Date;
import java.util.List;

/**
 * @author Bruno
 * 
 */
public class Validator {

	private static final int[] pesoCPF = { 11, 10, 9, 8, 7, 6, 5, 4, 3, 2 };
	private static final int[] pesoCNPJ = { 6, 5, 4, 3, 2, 9, 8, 7, 6, 5, 4, 3,	2 };

	public static boolean isEmptyOrNull(String s) {
		return (s == null || "".equals(s.trim()) || "null".equals(s.trim()));
	}

	public static boolean isNull(Date d) {
		return (d == null);
	}
	
	public static boolean isEmptyNullOrZero(Character c) {
		return (c == null || "".equals(c.toString()));
	}

	public static boolean isEmptyOrNull(Short s) {
		return (s == null || "".equals(s));
	}		

	public static boolean isEmptyOrNull(Integer s) {
		return (s == null);
	}
	
	public static boolean isEmptyOrNull(Long s) {
		return (s == null);
	}
	
	public static boolean isEmptyOrNull(Double s) {
		return (s == null);
	}

	public static boolean isEmptyOrNull(List l) {
		return (l == null || l.size() == 0);
	}

	public static boolean isEmptyNullOrZero(Integer s) {
		return (s == null || s.equals(0));
	}
	public static boolean isEmptyNullOrZero(Long s) {
		return (s == null || s.equals(0));
	}	
	public static boolean isEmptyNullOrZero(String s) {
		return (s == null || "".equals(s.trim()) || s.trim().equals("0") || "null".equals(s.trim()));
	}

	public static boolean isEmptyNullOrZero(Double s) {
		return (s == null || s == 0.0);
	}

	public static boolean isValidCPF(String cpf) {
		cpf = cpf.replace('.', ' ');
		cpf = cpf.replace('-', ' ');
		cpf = cpf.replaceAll(" ", "");

		if ((cpf == null) || (cpf.length() != 11))
			return false;
		
		if (cpf.equals("00000000000") || cpf.equals("11111111111") || cpf.equals("22222222222") || cpf.equals("33333333333") || cpf.equals("44444444444") || cpf.equals("55555555555") || cpf.equals("66666666666") || cpf.equals("77777777777") || cpf.equals("88888888888") || cpf.equals("99999999999"))
	        return false;		

		Integer digito1 = calcularDigito(cpf.substring(0, 9), pesoCPF);
		Integer digito2 = calcularDigito(cpf.substring(0, 9) + digito1, pesoCPF);
		return cpf.equals(cpf.substring(0, 9) + digito1.toString() + digito2.toString());
	}

	public static boolean isValidCNPJ(String cnpj) {
		cnpj = cnpj.replace('.', ' ');
		cnpj = cnpj.replace('/', ' ');
		cnpj = cnpj.replace('-', ' ');
		cnpj = cnpj.replaceAll(" ", "");

		if ((cnpj == null) || (cnpj.length() != 14))
			return false;

		Integer digito1 = calcularDigito(cnpj.substring(0, 12), pesoCNPJ);
		Integer digito2 = calcularDigito(cnpj.substring(0, 12) + digito1, pesoCNPJ);
		return cnpj.equals(cnpj.substring(0, 12) + digito1.toString() + digito2.toString());
	}
	
	private static int calcularDigito(String str, int[] peso) {
		int soma = 0;
		for (int indice = str.length() - 1, digito; indice >= 0; indice--) {
			digito = Integer.parseInt(str.substring(indice, indice + 1));
			soma += digito * peso[peso.length - str.length() + indice];
		}
		soma = 11 - soma % 11;
		return soma > 9 ? 0 : soma;
	}
	
	public static boolean in(String value, String... types) {
		for (String s : types) {
			if (s.equals(value)) {
				return true;
			}			
		}
		return false;
	}
	
	public static void main(String[] args) {
		Validator.isValidCPF("111.111.111-11");
	}

}
