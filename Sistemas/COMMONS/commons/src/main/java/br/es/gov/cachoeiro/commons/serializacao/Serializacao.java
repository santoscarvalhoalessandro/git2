package br.es.gov.cachoeiro.commons.serializacao;


import java.beans.XMLDecoder;
import java.beans.XMLEncoder;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectInputStream;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;
import java.io.Serializable;


public class Serializacao {
	
	@SuppressWarnings("resource")
	public static String serializaXML(Serializable o) {
		
		ByteArrayOutputStream bos = new ByteArrayOutputStream();
		XMLEncoder xmlEncoder = new XMLEncoder(bos);  
		try {				
			xmlEncoder.writeObject(o);	
			xmlEncoder.flush();
			
		} catch (Throwable e1) {
			e1.printStackTrace();
		}		
		return bos.toString();				
	}
	
	@SuppressWarnings("resource")
	public static Serializable deserializaXML(String o) {		
		XMLDecoder xmlDecoder = new XMLDecoder(new ByteArrayInputStream(o.getBytes()));  
		return (Serializable) xmlDecoder.readObject();  		
	}
	
	public static String serializaBin(Serializable o) {
		
		ByteArrayOutputStream bos = new ByteArrayOutputStream();
		ObjectOutput out = null;
		byte[] bytes = null;
		try {
			out = new ObjectOutputStream(bos);
			out.writeObject(o);
			bytes = bos.toByteArray();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				out.close();
				bos.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return new String(bytes);				
	}
	
	public static Object deserializaBin(String o) {		
		ByteArrayInputStream bis = new ByteArrayInputStream(o.getBytes());
		ObjectInput in = null;
		Object obj = null;
		try {
			in = new ObjectInputStream(bis);
			obj = in.readObject();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				bis.close();
				in.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		return obj;
	}
	
	
}
