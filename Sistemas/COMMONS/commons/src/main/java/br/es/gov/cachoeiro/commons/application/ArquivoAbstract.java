package br.es.gov.cachoeiro.commons.application;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public abstract class ArquivoAbstract {
	
	public abstract String trataLinha(String linha);
	public abstract boolean retiraLinha(String linha);
	
	public String lerArquivo(File file) {
		if (!file.exists()) {
			System.out.println("Arquivo não existe!");
			return "";
		}
		
    	FileReader fr;
		String str = "";
		try {
			fr = new FileReader(file);
			BufferedReader in = new BufferedReader(fr);
			while (in.ready()) {
				String str1 = in.readLine();
               	if (!retiraLinha(str1)) { 
               		str +=  trataLinha(str1)+ "\r\n";
               	}	
            }
            in.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return str;
	}
    public void gerarArquivo(String caminho, String conteudo) {
    	gerarArquivo(caminho, conteudo, false);
    }

    public void gerarArquivo(String caminho, String conteudo, boolean append) {
        try {
            FileWriter fw = new FileWriter(caminho);
            BufferedWriter out = new BufferedWriter(fw);
			out.write(conteudo);
	        out.close();
	        //System.out.println("Arquivo "+caminho+ " gerado com sucesso.");	        
	        
		} catch (IOException e) {
			e.printStackTrace();
		}
	}	
    

	public static void copia(File src, File dst) throws IOException {
        InputStream in = new FileInputStream(src);
        OutputStream out = new FileOutputStream(dst);
        byte[] buf = new byte[1024];
        int len;
        while ((len = in.read(buf)) > 0) {
            out.write(buf, 0, len);
        }
        in.close();
        out.close();
    }
	
}
