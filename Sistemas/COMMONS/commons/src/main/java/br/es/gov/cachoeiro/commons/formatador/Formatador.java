package br.es.gov.cachoeiro.commons.formatador;


import java.text.SimpleDateFormat;
import java.util.Date;

import br.es.gov.cachoeiro.commons.validator.Validator;

public class Formatador {

	public static String FormatarData(Date data, String pattern) {
		if (data != null) {
			SimpleDateFormat sdf = new SimpleDateFormat(pattern);
			return sdf.format(data);
		}
		return null;
	}

	public static String cpfCNPJSemFormatacao(String doc) {
		if (doc == null) {
			return doc;
		}
		return doc.replace(".", "").replace("-", "").replace("/", "");
	}
	
	public static String ajustaTamanhoString(int tamanho, String str, String carac, boolean antes) {
		while (str.length() < tamanho) {
			if (antes) {
				str = carac + str;
			} else {
				str += carac;
			}
		}
		return str;		
	}

	public static String formatarCNPJ(String doc) {
		if (doc == null) {
			return doc;
		}

		if (doc.length() < 14) {
			return "CNPJ invalido.";
		}
		doc = doc.substring(0, 2) + "." + doc.substring(2, 5) + "."
				+ doc.substring(5, 8) + "/" + doc.substring(8, 12) + "-"
				+ doc.substring(12, 14);
		return doc;
	}

	public static String formatarCPF(String doc) {
		if (Validator.isEmptyOrNull(doc)) {
			return doc;
		}

		if (doc.length() < 11)
			return "CPF invalido.";
		
		if (doc.contains(".") || doc.contains("-"))
			return doc;
			
		doc = doc.substring(0, 3) + "." + doc.substring(3, 6) + "."
				+ doc.substring(6, 9) + "-" + doc.substring(9, 11);
		return doc;
	}

	public static String telefoneSemFormatacao(String doc) {
		if (Validator.isEmptyOrNull(doc)) {
			return doc;
		}
		return doc.replace("(", "").replace(")", "").replace("-", "");
	}

	public static String formatarTelefone(String doc) {
		if (Validator.isEmptyOrNull(doc)) {
			return doc;
		}

		if (doc.length() < 8)
			return "Telefone invalido.";
		
		if (doc.contains("(") || doc.contains(")") || doc.contains("-"))
			return doc;

		// INCLUI O DDD NO TELEFONE
		if ("3".equals(doc.substring(0, 1)) || "9".equals(doc.substring(0, 1)) || "8".equals(doc.substring(0, 1)))
			doc = "28"+doc;
			
		doc = "(" + doc.substring(0, 2) + ")" + doc.substring(2, 6) + "-"
				+ doc.substring(6);
		return doc;
	}
	
	public static String CEPSemFormatacao(String doc) {
		if (Validator.isEmptyOrNull(doc)) {
			return doc;
		}
		return doc.replace(".", "").replace("-", "");
	}

	public static String formatarCEP(String doc) {
		if (Validator.isEmptyOrNull(doc)) {
			return doc;
		}

		if (doc.length() < 8)
			return "CEP invalido.";
		
		if (doc.contains(".") || doc.contains("-"))
			return doc;
			
		doc = doc.substring(0, 2) + "." + doc.substring(2, 5) + "-"
				+ doc.substring(5, 8);
		return doc;
	}

	public static String acertaLogin(String login) {
		
		if(login.length() > 10){
			login = login.substring(0, 10);
		}
		
		int tamanhoLogin = (10 - login.length()); 
		
		for (int x = 0; x < tamanhoLogin; x++) {
			login = login + " ";
		}
		return login;
	}
		
	public static String acertaOrgao(String orgao) {
		int tamanhoOrg = (15 - orgao.length()); 
		
		for (int x = 0; x < tamanhoOrg; x++) {
			orgao = orgao + " ";
		}
		return orgao;
	}	
	
	public static String acertaOrgaoCD(String orgao) {
		int tamanhoOrg = (20 - orgao.length()); 
		
		for (int x = 0; x < tamanhoOrg; x++) {
			orgao = orgao + " ";
		}
		return orgao;
	}	
	
}

