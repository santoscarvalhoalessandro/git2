package br.es.gov.cachoeiro.commons.zip;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Enumeration;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipException;
import java.util.zip.ZipFile;
import java.util.zip.ZipOutputStream;

public class Zipper {

	private static final int TAMANHO_BUFFER = 2048; // 2 Kb
	
	public static void main(String[] args) throws ZipException, IOException {
		unzipFile(new File("D:\\ST\\sistemas\\Aprender\\aprender-Client\\dist\\aprender-Client.zip"), 
		new File("C:\\Documents and Settings\\bruno.CACHOEIRO\\Desktop\\LiberadorDeVersão\\tmp\\"));
		
	}
	

	public static void unzipFile(File arquivoZip, File diretorio)
			throws ZipException, IOException {
		ZipFile zip = null;
		File arquivo = null;
		InputStream is = null;
		OutputStream os = null;
		byte[] buffer = new byte[TAMANHO_BUFFER];
		try {
			if (!diretorio.exists()) {
				diretorio.mkdirs();
			}
			if (!diretorio.exists() || !diretorio.isDirectory()) {
				throw new IOException("Informe um diretório válido");
			}
			zip = new ZipFile(arquivoZip);
			Enumeration e = zip.entries();
			while (e.hasMoreElements()) {
				ZipEntry entrada = (ZipEntry) e.nextElement();
				arquivo = new File(diretorio, entrada.getName());

				if (entrada.isDirectory() && !arquivo.exists()) {
					arquivo.mkdirs();
					continue;
				}

				if (!arquivo.getParentFile().exists()) {
					arquivo.getParentFile().mkdirs();
				}

				try {
					is = zip.getInputStream(entrada);
					os = new FileOutputStream(arquivo);
					int bytesLidos = 0;
					if (is == null) {
						throw new ZipException("Erro ao ler a entrada do zip: "
								+ entrada.getName());
					}
					while ((bytesLidos = is.read(buffer)) > 0) {
						os.write(buffer, 0, bytesLidos);
					}
				} finally {
					if (is != null) {
						try {
							is.close();
						} catch (Exception ex) {
						}
					}
					if (os != null) {
						try {
							os.close();
						} catch (Exception ex) {
						}
					}
				}
			}
		} finally {
			if (zip != null) {
				try {
					zip.close();
				} catch (Exception e) {
				}
			}
		}
	}

	public static void zipFile(File dir2zip, File zipFile) throws IOException {
		ZipOutputStream outputStream = new ZipOutputStream(
				new FileOutputStream(zipFile));
		zipDir(dir2zip, outputStream);
		outputStream.close();
	}
	public static void zipFile(List<File> files, File zipFile) throws IOException {
		ZipOutputStream outputStream = new ZipOutputStream(
				new FileOutputStream(zipFile));
		_zip(files, outputStream);
		outputStream.close();
	}

	private static void zipDir(File dir2zip, ZipOutputStream zos)
			throws IOException {
		_zip(dir2zip, zos, dir2zip.getPath());
	}

	/** implementacao */
	private static void _zip(File dir2zip, ZipOutputStream zos, String path)
			throws IOException, FileNotFoundException {
		File[] dirList = dir2zip.listFiles();
		byte[] readBuffer = new byte[512];
		int bytesIn = 0;
		for (File f : dirList) {
			if (f.isDirectory()) {
				_zip(f, zos, path);
				continue;
			}
			FileInputStream fis = new FileInputStream(f);
			ZipEntry anEntry = new ZipEntry(f.getPath().substring(
					path.length() + 1));
			zos.putNextEntry(anEntry);
			while ((bytesIn = fis.read(readBuffer)) != -1) {
				zos.write(readBuffer, 0, bytesIn);
			}
			fis.close();
		}
	}
	private static void _zip(List<File> files, ZipOutputStream zos)
			throws IOException, FileNotFoundException {
		byte[] readBuffer = new byte[512];
		int bytesIn = 0;
		for (File f : files) {
			if (f.isDirectory()) {
				_zip(f, zos, f.getPath());
				continue;
			}
			FileInputStream fis = new FileInputStream(f);
			ZipEntry anEntry = new ZipEntry(f.getName());
			zos.putNextEntry(anEntry);
			while ((bytesIn = fis.read(readBuffer)) != -1) {
				zos.write(readBuffer, 0, bytesIn);
			}
			fis.close();
		}
	}

}