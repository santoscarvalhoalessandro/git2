package br.es.gov.cachoeiro.commons.converter;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class Conversor {

	public static String retornaSiglaDiaSemana(int i){
		String s;
		
		switch (i) {
			case 1 :
				s = "DOM";
				break;
			case 2 :
				s = "SEG";
				break;
			case 3 :
				s = "TER";
				break;
			case 4 :
				s = "QUA";
				break;
			case 5 :
				s = "QUI";
				break;
			case 6 :
				s = "SEX";
				break;
			case 7 :
				s = "SAB";
				break;
			default:
				s = "";
				break;
		}
		return s;
	}
	
	public static String converteData(String pattern, Date valor){
		if(valor == null) {
			return null;
		}
		DateFormat df = new SimpleDateFormat(pattern);
		String d = df.format(valor);
		return d;
	}
	
	public static Date converteData(String pattern, String valor){
		Date d = null;
		try {
			if(valor == null || valor.equals("")){
				return null;
			}
			DateFormat df = new SimpleDateFormat(pattern);		
			d = df.parse(valor);
			
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return d;
	}
	
	
	/*public static String converteDiaMesAno(Date data) {
		if(data == null) {
			return null;
		}
		DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
		String d = df.format(data);
		return d;
	}	
	
	public static Date converteDiaMesAno(String s){
		Date d = null;
		try {
			if(s == null || s.equals("")){
				return null;
			}
			DateFormat df = new SimpleDateFormat("dd/MM/yyyy");		
			d = df.parse(s);
			
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return d;
	}
	
	public static Date converteDiaMesAnoHoraMinutoSegundo(String s){
		Date d = new Date();
		try {
			if(s == null || s.equals("")){
				return null;
			}		
			DateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");				
			d = df.parse(s);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return d;
	}
	
	public static String converteDiaMesAnoHoraMinutoSegundo(Date data) {
		if(data == null) {
			return null;
		}
		DateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		String d = df.format(data);
		return d;
	}	
	
	public static Date converteAnoMesDiaHoraMinuto(String s){
		Date d = new Date();
		try {
			if(s == null || s.equals("")){
				return null;
			}		
			DateFormat df = new SimpleDateFormat("yyyyMMddHHmm");				
			d = df.parse(s);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return d;
	}
	
	public static String converteHoraMinutoSegundo(Date data) {
		if(data == null) {
			return null;
		}
		DateFormat df = new SimpleDateFormat("HH:mm:ss");
		String d = df.format(data);
		return d;
	}
	
	public static String converteHoraMinuto(Date data) {
		if(data == null) {
			return null;
		}
		DateFormat df = new SimpleDateFormat("HH:mm");
		String d = df.format(data);
		return d;
	}
	
	public static String converteAnoMesDiaHoraMinutoSegundoMilissegundo(Date data) {
		if(data == null) {
			return null;
		}
		DateFormat df = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss.SSS");
		String d = df.format(data);
		return d;
	}
	
	public static String converteHoraMinutoSegundoMilissegundo(Date data) {
		if(data == null) {
			return null;
		}
		DateFormat df = new SimpleDateFormat("HH:mm:ss.SSS");
		String d = df.format(data);
		return d;
	}
			
	public static Date converteHoraMinuto(Calendar c){
		if(c == null) {
			return null;
		}					
		return c.getTime();
	}
	
	public static DateTime converteHoraMinuto(String s) {    			
		DateTime d = null;
		try {
			SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");
			String hora = s;
			d = new DateTime(sdf.parseObject(hora));
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return d;
    }*/
	
	public static int TimeToInt(String vl) {
		int hr = Integer.valueOf(vl.split(":")[0]);
		int mi = Integer.valueOf(vl.split(":")[1]);
		  
		return mi + (hr*60);
	} 

	public static String IntToTime(int val) {
		int hr = Math.round(val / 60);
		int mins = Math.round((val -((Math.round(val/60) * 60) )));

		String res = "";
		
		if (hr < 10) {
			res = "0" + hr;
		} else {
			res = ""+hr;
		}
		
		if (mins < 10) {
			res += ":0" + mins;
		} else {
			res += ":" + mins;
		}
		
		return res;

	}
	
	/*public static String formatDate(Date dt) {
		if (dt == null) {
			return "";
		}
		return new SimpleDateFormat("dd/MM/yyyy").format(dt);
	}
	
	public static String formatDateTime(Date dt) {
		if (dt == null) {
			return "";
		}
		return new SimpleDateFormat("dd/MM/yyyy HH:mm:ss").format(dt);
	}*/
	
	public static String formatarMoeda(Double d){
		return String.format("%.2f", d);
	}
			
	public static int compareDates(Date dt1, Date dt2) {
    	Calendar c1 = Calendar.getInstance();
		c1.setTime(dt1);
		Calendar c2 = Calendar.getInstance();
		c2.setTime(dt2);
		
		long span = c2.getTimeInMillis() - c1.getTimeInMillis();		
		
		Calendar c3 = Calendar.getInstance();
	    c3.setTimeInMillis(span);
	    	    
	    return (int) c3.getTimeInMillis()/1000; // retorna quantos segundos tem um intervalo de data/ hora 
    }
	
	
	public static void main(String[] args) {
		
		Double d = 39.199999999999996;
		System.out.println(String.format("%.2f", new BigDecimal(d)));
						
		System.out.println(converteData("yyyyMMddHHmm", "201501012217"));
		//String s = converteAnoMesDiaHoraMinutoSegundoMilissegundo(d);
		//System.out.println(s.replace("/", "").replace(" ", "").replace(":", "").replace(".", ""));
		//System.out.println(converteAnoMesDiaHoraMinutoSegundoMilissegundo(d).replace("/", "").replace(" ", "").replace(":", "").replace(".", ""));
		//System.out.println(converteHoraMinutoSegundoMilissegundo(d).replace(":", "").replace(".", ""));
	}
	
	
}

