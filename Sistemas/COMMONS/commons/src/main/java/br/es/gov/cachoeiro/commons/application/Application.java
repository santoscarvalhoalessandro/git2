package br.es.gov.cachoeiro.commons.application;

import java.io.InputStream;
import org.omg.CORBA.Environment;

public class Application {

	public static boolean isValidFile(String resource) {
		boolean retorno = false;
		String stripped = resource.startsWith("/") ?
				resource.substring(1) : resource;

		InputStream stream = null;
		ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
		if (classLoader!=null) {
			stream = classLoader.getResourceAsStream( stripped );
		}

		if ( stream == null ) {
			stream = Environment.class.getResourceAsStream( resource );
		}
		
		if ((stream != null) || (Environment.class.getClassLoader() != null)) {
			if ( stream == null ) {
				stream = Environment.class.getClassLoader().getResourceAsStream( stripped );
			} 
			retorno = (stream != null);
		}	
		
		if ( stream == null ) {
			retorno = false;
		}
		return retorno;
	}

}
