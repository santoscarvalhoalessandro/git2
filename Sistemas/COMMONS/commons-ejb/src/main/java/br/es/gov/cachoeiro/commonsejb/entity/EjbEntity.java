package br.es.gov.cachoeiro.commonsejb.entity;

import java.io.Serializable;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

public class EjbEntity implements Serializable {

	private static final String SUFIX_NAME_CLASS1 = "EjbEntity";
	private static final String SUFIX_NAME_CLASS2 = "TO";

	public EjbEntity() {
	}

	public EjbEntity(Serializable jpaEntity, int deep) throws Exception {
		assign(jpaEntity, deep);
	}

	public void assign(Serializable jpaEntity, int deep) throws Exception {
		//System.out.println("Class: " + jpaEntity.getClass());

		assignFindingEntity(jpaEntity, this, deep);
	}

	/**
	 * @param jpaEntity
	 * @param ejbEntity
	 * @return
	 * @throws InstantiationException
	 * @throws IllegalAccessException
	 * @throws ClassNotFoundException
	 * @throws IllegalArgumentException
	 * @throws InvocationTargetException
	 * @author Bruno
	 * @see Metodo para ser utilizado pela classe EjbEntity passando this;
	 * 
	 */
	private static EjbEntity assignFindingEntity(Serializable jpaEntity,
			EjbEntity ejbEntity, int deep) throws InstantiationException,
			IllegalAccessException, ClassNotFoundException,
			IllegalArgumentException, InvocationTargetException {
		String className = jpaEntity.getClass().getName();

		if (isValidEntity(className)) {
			return assignTo(jpaEntity, ejbEntity, deep);
		}

		return null;
	}

	public static EjbEntity assignFindingEntity(Serializable jpaEntity, int deep)
			throws InstantiationException, IllegalAccessException, ClassNotFoundException, IllegalArgumentException, InvocationTargetException {
		String className = jpaEntity.getClass().getName();

		if (isValidEntity(className)) {
			EjbEntity ejbEntity = getEjbEntityByJpaName(className);
			return assignTo(jpaEntity, ejbEntity, deep);
		}

		return null;
	}

	private static EjbEntity getEjbEntityByJpaName(String jpaName)
			throws InstantiationException, IllegalAccessException,ClassNotFoundException {
		return (EjbEntity) Class.forName(getClassName(jpaName)).newInstance();
	}

	private static boolean isValidEntity(String jpaName) {
		boolean result = false;
		try {
			Class.forName(getEjbClassName(jpaName));

			result = true;
		} catch (ClassNotFoundException e) {
			try {
				Class.forName(getTOClassName(jpaName));

				result = true;
			} catch (ClassNotFoundException e2) {
				try {
					if (Class.forName(jpaName).getSuperclass() != null && Class.forName(jpaName).getSuperclass().getName().contains("Entity")) {
						result = true;							
					}
				} catch (ClassNotFoundException e1) {
				}
			}
			
		}

		return result;
	}

	private static EjbEntity assignTo(Serializable jpaEntity,
			EjbEntity ejbEntity, int deep) throws IllegalArgumentException,
			IllegalAccessException, InvocationTargetException,
			InstantiationException, ClassNotFoundException {

		if (deep == 0) {
			return ejbEntity;
		}

		Method[] methods = jpaEntity.getClass().getMethods();

		for (int i = 0; i < methods.length; i++) {
			if (methods[i].getName().substring(0, 3).equals("get")) {
				String nome = "set"	+ methods[i].getName().substring(3,	methods[i].getName().length());
				try {
					String nameJpaEntity = methods[i].getReturnType().getName();
					String nameEjbEntity = getClassName(methods[i].getReturnType().getName());

					boolean primitivo = methods[i].getReturnType().isPrimitive();

					Class clazz = null;
					if (!primitivo) {
						clazz = Class.forName(isValidEntity(nameJpaEntity) ? nameEjbEntity : nameJpaEntity);
					}

					Method methodSet = ejbEntity.getClass().getMethod(nome,	new Class[] { primitivo ? methods[i].getReturnType() : clazz });

					Object obj = null;
					try {
						obj = methods[i].invoke(jpaEntity, null);
						if (methods[i].getReturnType().getSimpleName().equals("List")) {
							obj = assignList((List) obj, 1);

						} else if (obj != null && isValidEntity(methods[i].getReturnType().getName())) {
							obj = assignFindingEntity((Serializable) obj, deep - 1);
						}
					} catch (RuntimeException ex) {
						obj = null;
					}

					methodSet.invoke(ejbEntity, new Object[] { obj });
				} catch (NoSuchMethodException ex) {
				}
			}
		}

		return ejbEntity;
	}

	private static List assignList(List list, int deep)
			throws IllegalArgumentException, InstantiationException,
			IllegalAccessException, ClassNotFoundException,
			InvocationTargetException {
		List listaDestino = new ArrayList();

		for (int x = 0; x < list.size(); x++) {
			listaDestino.add(assignFindingEntity((Serializable) list.get(x), deep));
		}

		return listaDestino;
	}

	private static String getEjbClassName(String jpaEntityName) {
		String subpack = "ejb.";

		String name = jpaEntityName;
		name = name.substring(0, name.lastIndexOf(".") + 1) + subpack
				+ name.substring(name.lastIndexOf(".") + 1, name.length())
				+ SUFIX_NAME_CLASS1;
		return name;
	}
	private static String getTOClassName(String jpaEntityName) {

		if (jpaEntityName.lastIndexOf(".") == -1) {
			return "";
		}
		
		String subpack = "pojo.";

		String name = jpaEntityName;
		String classe = name.substring(name.lastIndexOf(".") + 1, name.length());
		name = name.substring(0, name.lastIndexOf("."));
		
		name = name.substring(0, name.lastIndexOf(".") + 1) + subpack
				+ classe
				+ SUFIX_NAME_CLASS2;
		return name;
	}
	
	private static String getClassName(String jpaName) {
		boolean result = false;
		try {
			Class.forName(getEjbClassName(jpaName));

			return getEjbClassName(jpaName);
		} catch (ClassNotFoundException e) {
			try {
				Class.forName(getTOClassName(jpaName));

				return getTOClassName(jpaName);
			} catch (ClassNotFoundException e2) {
				return jpaName;
			}
			
		}
	}


	public static List assignListEntity(List list, int deep) throws Exception{
		return assignList(list, deep);		
	}
	
}