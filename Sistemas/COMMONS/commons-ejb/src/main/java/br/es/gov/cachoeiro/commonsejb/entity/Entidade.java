package br.es.gov.cachoeiro.commonsejb.entity;

import java.io.Serializable;
import java.lang.reflect.Field;
import java.lang.reflect.Method;

import javax.persistence.Id;

public class Entidade implements Serializable{
	
	public static Serializable newInstance(Serializable entity) {
		try {
			if (isValidEntity(entity.getClass().getName())) {
				return assignNewToNull(entity);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return entity;
	}
	
	public static Serializable newInstance(Class clazz) {
		Serializable s = null;
		try {
			if (isValidEntity(clazz.getName())) {
					s = (Serializable) clazz.newInstance();
			} else {
				return null;
			}
			return assignTo(s);
		} catch (Exception e) {
			e.printStackTrace();
			return s;
		}
		
	} 
	
	private static String getEjbClassName(String EntityName) {
		
		String name = EntityName;
		name = name.substring(0, name.lastIndexOf(".") + 1) + name.substring(name.lastIndexOf(".") + 1, name.length());
		return name;
	}
	
	private static boolean isValidEntity(String name) {
		boolean result = false;
		try {
			Class.forName(getEjbClassName(name));
			if (name.contains(".entidades.")) {
				result = true;
			} 
		} catch (ClassNotFoundException e) {
		}

		return result;
	}
	
	private static Serializable assignTo(Serializable entity) throws Exception {
		
		Method[] methods = entity.getClass().getMethods();

		for (int i = 0; i < methods.length; i++) {
			if (methods[i].getName().substring(0, 3).equals("get")) {
				String nome = "set"	+ methods[i].getName().substring(3,	methods[i].getName().length());
				try {
					String nameEntity = methods[i].getReturnType().getName();
					//String nameEjbEntity = getEjbClassName(methods[i].getReturnType().getName());

					boolean primitivo = methods[i].getReturnType().isPrimitive();

					Class clazz = null;
					if (!primitivo && isValidEntity(nameEntity)) {
						clazz = Class.forName(nameEntity);
						Method methodSet = entity.getClass().getMethod(nome, new Class[] { primitivo ? methods[i].getReturnType() : clazz });
						Object obj = clazz.newInstance();
						methodSet.invoke(entity, new Object[] { obj });
					}

				} catch (NoSuchMethodException ex) {
				}
			}
		}

		return entity;
	}
	
	

	public static Serializable eliminaReferenciasNulas(Serializable entity) throws Exception{
		
		Method[] methods = entity.getClass().getMethods();
		//System.out.println("methods "+ methods);

		for (int i = 0; i < methods.length; i++) {
			try {
				if (methods[i].getName().substring(0, 3).equals("get")) {
					String nome = "set"	+ methods[i].getName().substring(3,	methods[i].getName().length());
					String nameEntity = methods[i].getReturnType().getName();
					//System.out.println("nameentity "+ nameEntity);
					String nameEjbEntity = getEjbClassName(methods[i].getReturnType().getName());

					boolean primitivo = methods[i].getReturnType().isPrimitive();

					if (!primitivo && isValidEntity(nameEntity)) {
						
						Method meth = entity.getClass().getMethod(methods[i].getName());
						//System.out.println("meth " + meth);
						Object retobj = meth.invoke(entity);
						//System.out.println("retobj " + retobj);
						
						Object o = fieldValueByAnnotation((Serializable)retobj, Id.class);						
						
						if (o == null || o.equals(0)) {
							Method methodSet = entity.getClass().getMethod(nome, new Class[] { meth.getReturnType() });							
							methodSet.invoke(entity, new Object[] { null });							
						}
					}
				}
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		return entity;
	}

	private static Serializable assignNewToNull(Serializable entity) throws Exception {
		Method[] methods = entity.getClass().getMethods();

		for (int i = 0; i < methods.length; i++) {
			if (methods[i].getName().substring(0, 3).equals("get")) {
				String nome = "set"	+ methods[i].getName().substring(3,	methods[i].getName().length());
				try {
					String nameEntity = methods[i].getReturnType().getName();
					//String nameEjbEntity = getEjbClassName(methods[i].getReturnType().getName());

					boolean primitivo = methods[i].getReturnType().isPrimitive();
					
					if (!primitivo && isValidEntity(nameEntity)) {
						
						Method meth = entity.getClass().getMethod(methods[i].getName());
						Object retobj = meth.invoke(entity);
						
						//System.out.println("Entidade:methods[i].getName(): "+methods[i].getName());
						
						if (retobj == null) {
							Method methodSet = entity.getClass().getMethod(nome, new Class[] { meth.getReturnType() });							
							methodSet.invoke(entity, new Object[] { meth.getReturnType().newInstance() });							
						}
						//meth = entity.getClass().getMethod(methods[i].getName());
						//retobj = meth.invoke(entity);
						
					}

				} catch (NoSuchMethodException ex) {
					ex.printStackTrace();
				}
			}
		}

		return entity;
	}

	
	private static Object fieldValueByAnnotation(Serializable entity, Class annotationClass) {
		Object retorno = null;
		try {
			//System.out.println("fieldValueByAnnotation.entity: "+entity);
			Class classe = entity.getClass();
			Field[] fields = classe.getDeclaredFields();
			for (Field field : fields) {
				if (field.isAnnotationPresent(annotationClass)) {
					retorno = getValue(classe, entity, field);
					break;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return retorno;
	}
	
	private static Object getValue(Class clazz, Object object, Field field)
			throws Exception {
		String methodGet = "get"
				+ field.getName().substring(0, 1).toUpperCase()
				+ field.getName().substring(1);

		Method meth = clazz.getMethod(methodGet);
		
		if (meth.getReturnType().getName().contains("List")) {
			return "List";			
		}
		
		
		Object retobj = meth.invoke(object);		
		
		return retobj;
	}
	
}
